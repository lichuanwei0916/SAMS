/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 80032
 Source Host           : localhost:3306
 Source Schema         : sams

 Target Server Type    : MySQL
 Target Server Version : 80032
 File Encoding         : 65001

 Date: 10/10/2023 09:14:32
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for athlete
-- ----------------------------
DROP TABLE IF EXISTS `athlete`;
CREATE TABLE `athlete`  (
  `A_ID` int(0) NOT NULL AUTO_INCREMENT COMMENT '运动员编号',
  `A_Name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '运动员姓名',
  `A_No` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '运动员工号/学号',
  `A_Gender` varchar(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '运动员性别',
  `A_Birth` date NULL DEFAULT NULL COMMENT '运动员生日',
  `A_Tel` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '运动员联系方式',
  `A_College_ID` int(0) NOT NULL COMMENT '运动员所属学院（单位）字段',
  `A_Login_ID` int(0) NOT NULL COMMENT '运动员登录ID字段',
  `A_Group_ID` int(0) NOT NULL COMMENT '运动员所属组别ID字段',
  PRIMARY KEY (`A_ID`) USING BTREE,
  INDEX `A_College_ID`(`A_College_ID`) USING BTREE,
  INDEX `A_Login_ID`(`A_Login_ID`) USING BTREE,
  INDEX `A_Group_ID`(`A_Group_ID`) USING BTREE,
  CONSTRAINT `athlete_ibfk_1` FOREIGN KEY (`A_College_ID`) REFERENCES `college` (`C_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `athlete_ibfk_2` FOREIGN KEY (`A_Login_ID`) REFERENCES `login_account` (`L_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `athlete_ibfk_3` FOREIGN KEY (`A_Group_ID`) REFERENCES `groupz` (`G_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of athlete
-- ----------------------------
INSERT INTO `athlete` VALUES (1, '李传伟', '21206112', '男', '2003-09-17', '13677079328', 15, 1, 1);
INSERT INTO `athlete` VALUES (2, '康良俊', '21206109', '男', '2003-09-01', '17113470076', 15, 3, 1);
INSERT INTO `athlete` VALUES (3, '彭斌', '21012515', '男', '2003-06-02', '18179569817', 1, 4, 1);
INSERT INTO `athlete` VALUES (4, '匡科利', '21206110', '男', '2001-08-21', '19179146969', 15, 2, 1);

-- ----------------------------
-- Table structure for basicinformation
-- ----------------------------
DROP TABLE IF EXISTS `basicinformation`;
CREATE TABLE `basicinformation`  (
  `BI_StartTime` datetime(0) NULL DEFAULT NULL,
  `BI_EndTime` datetime(0) NULL DEFAULT NULL,
  `BI_People_Litmit` int(0) NULL DEFAULT NULL,
  `BI_Item_Litmit` int(0) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for college
-- ----------------------------
DROP TABLE IF EXISTS `college`;
CREATE TABLE `college`  (
  `C_ID` int(0) NOT NULL AUTO_INCREMENT COMMENT '学院ID，各学院（单位）编号',
  `C_NAME` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '学院名称,各学院（单位）名字\r\n学院名称,各学院（单位）名字\r\n学院名称,各学院（单位）名字\r\n学院名称,各学院（单位）名字\r\n学院名称,各学院（单位）名字\r\n学院名称,各学院（单位）名字\r\n学院名称,各学院（单位）名字\r\n学院名称,各学院（单位）名字\r\n学院名称,各学院（单位）名字\r\n学院名称,各学院（单位）名字\r\n学院名称，各学院（单位）名字',
  PRIMARY KEY (`C_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of college
-- ----------------------------
INSERT INTO `college` VALUES (1, '材料科学与工程学院');
INSERT INTO `college` VALUES (2, '环境与化学工程学院');
INSERT INTO `college` VALUES (3, '航空制造工程学院');
INSERT INTO `college` VALUES (4, '信息工程学院');
INSERT INTO `college` VALUES (5, '外国语学院');
INSERT INTO `college` VALUES (6, '数学与信息科学学院');
INSERT INTO `college` VALUES (7, '飞行器工程学院');
INSERT INTO `college` VALUES (8, '测试与光电工程学院');
INSERT INTO `college` VALUES (9, '经济管理学院');
INSERT INTO `college` VALUES (10, '体育学院');
INSERT INTO `college` VALUES (11, '土木建筑学院');
INSERT INTO `college` VALUES (12, '艺术与设计学院');
INSERT INTO `college` VALUES (13, '文法学院');
INSERT INTO `college` VALUES (14, '音乐学院');
INSERT INTO `college` VALUES (15, '软件学院');
INSERT INTO `college` VALUES (16, '国际教育学院');
INSERT INTO `college` VALUES (17, '机关');
INSERT INTO `college` VALUES (18, '图书馆');
INSERT INTO `college` VALUES (19, '工训中心');
INSERT INTO `college` VALUES (20, '资产公司');
INSERT INTO `college` VALUES (21, '南昌航空大学医院');

-- ----------------------------
-- Table structure for competitioninformation
-- ----------------------------
DROP TABLE IF EXISTS `competitioninformation`;
CREATE TABLE `competitioninformation`  (
  `CI_ID` int(0) NOT NULL AUTO_INCREMENT COMMENT '比赛ID',
  `CI_Venue` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '比赛地点',
  `CI_TIME` datetime(0) NOT NULL COMMENT '比赛时间',
  `CI_Issue` int(0) NOT NULL DEFAULT 0 COMMENT '该比赛成绩是否发布（0为未发布，1为已发布）',
  `SI_ID` int(0) NULL DEFAULT NULL COMMENT '比赛对应项目ID',
  `CI_Limit` int(0) NULL DEFAULT NULL COMMENT '比赛每组限制人数,若为空，代表无限制',
  `CI_Nature` int(0) NULL DEFAULT NULL COMMENT '比赛性质,1为预赛，2为决赛，3为预决赛',
  `G_ID` int(0) NULL DEFAULT NULL COMMENT '比赛组别ID',
  PRIMARY KEY (`CI_ID`) USING BTREE,
  INDEX `G_ID`(`G_ID`) USING BTREE,
  INDEX `competitioninformation_ibfk_1`(`SI_ID`) USING BTREE,
  CONSTRAINT `competitioninformation_ibfk_1` FOREIGN KEY (`SI_ID`) REFERENCES `sportitem` (`SI_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `competitioninformation_ibfk_2` FOREIGN KEY (`G_ID`) REFERENCES `groupz` (`G_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of competitioninformation
-- ----------------------------
INSERT INTO `competitioninformation` VALUES (3, '田径场', '2023-05-30 10:00:00', 1, 1, 8, 1, 1);
INSERT INTO `competitioninformation` VALUES (5, '田径场', '2023-05-28 10:01:00', 0, 1, 8, 1, 2);
INSERT INTO `competitioninformation` VALUES (6, '田径场', '2023-06-16 11:00:00', 0, 3, 8, 1, 1);
INSERT INTO `competitioninformation` VALUES (7, '田径场', '2023-06-02 10:00:00', 0, 1, 8, 2, 1);
INSERT INTO `competitioninformation` VALUES (9, '田径场', '2023-06-30 06:00:00', 0, 4, 2, 1, 2);
INSERT INTO `competitioninformation` VALUES (10, '田径场西侧', '2023-06-30 03:00:00', 0, 10, 8, 3, 1);

-- ----------------------------
-- Table structure for finalsinformation
-- ----------------------------
DROP TABLE IF EXISTS `finalsinformation`;
CREATE TABLE `finalsinformation`  (
  `FI_ID` int(0) NOT NULL AUTO_INCREMENT COMMENT '自增长ID',
  `CI_ID` int(0) NOT NULL COMMENT '比赛的ID',
  `A_ID` int(0) NOT NULL COMMENT '参加比赛的运动员ID',
  `R_ID` int(0) NULL DEFAULT NULL COMMENT '记录成绩的裁判长ID',
  `I_ID` int(0) NULL DEFAULT NULL COMMENT '检录员ID',
  `FI_RecordingStatus` int(0) NULL DEFAULT 0 COMMENT '检录状态',
  `FI_RecordingTime` datetime(0) NULL DEFAULT NULL COMMENT '检录时间',
  `FI_Group` int(0) NULL DEFAULT NULL COMMENT '组次',
  `FI_Runway` int(0) NULL DEFAULT NULL COMMENT '跑道',
  `FI_Grade` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '决赛成绩',
  PRIMARY KEY (`FI_ID`) USING BTREE,
  INDEX `A_ID`(`A_ID`) USING BTREE,
  INDEX `R_ID`(`R_ID`) USING BTREE,
  INDEX `I_ID`(`I_ID`) USING BTREE,
  INDEX `finalsinformation_ibfk_1`(`CI_ID`) USING BTREE,
  CONSTRAINT `finalsinformation_ibfk_1` FOREIGN KEY (`CI_ID`) REFERENCES `competitioninformation` (`CI_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `finalsinformation_ibfk_2` FOREIGN KEY (`A_ID`) REFERENCES `athlete` (`A_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `finalsinformation_ibfk_3` FOREIGN KEY (`R_ID`) REFERENCES `refereechief` (`R_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `finalsinformation_ibfk_4` FOREIGN KEY (`I_ID`) REFERENCES `inspector` (`I_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of finalsinformation
-- ----------------------------
INSERT INTO `finalsinformation` VALUES (1, 7, 1, NULL, NULL, 0, NULL, 1, 1, NULL);

-- ----------------------------
-- Table structure for groupz
-- ----------------------------
DROP TABLE IF EXISTS `groupz`;
CREATE TABLE `groupz`  (
  `G_ID` int(0) NOT NULL AUTO_INCREMENT COMMENT '各组别（单位）编号',
  `G_NAME` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '各组别（单位）名字',
  PRIMARY KEY (`G_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of groupz
-- ----------------------------
INSERT INTO `groupz` VALUES (1, '学生男子组');
INSERT INTO `groupz` VALUES (2, '学生女子组');
INSERT INTO `groupz` VALUES (3, '学生男女混合项目');
INSERT INTO `groupz` VALUES (4, '教师青年男子组');
INSERT INTO `groupz` VALUES (5, '教师青年女子组');
INSERT INTO `groupz` VALUES (6, '教师中年男子组');
INSERT INTO `groupz` VALUES (7, '教师中年女子组');
INSERT INTO `groupz` VALUES (8, '职工男女混合');
INSERT INTO `groupz` VALUES (9, '团体趣味项目');

-- ----------------------------
-- Table structure for headreferee
-- ----------------------------
DROP TABLE IF EXISTS `headreferee`;
CREATE TABLE `headreferee`  (
  `H_ID` int(0) NOT NULL AUTO_INCREMENT,
  `H_Name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `H_No` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `H_Gender` varchar(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `H_Age` int(0) NOT NULL,
  `H_Tel` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `H_Login_ID` int(0) NOT NULL,
  PRIMARY KEY (`H_ID`) USING BTREE,
  INDEX `H_Login_ID`(`H_Login_ID`) USING BTREE,
  CONSTRAINT `headreferee_ibfk_1` FOREIGN KEY (`H_Login_ID`) REFERENCES `login_account` (`L_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of headreferee
-- ----------------------------
INSERT INTO `headreferee` VALUES (1, '陈云', '71369', '女', 25, '13678562036', 5);
INSERT INTO `headreferee` VALUES (2, '张三', '72560', '男', 36, '13698536423', 6);

-- ----------------------------
-- Table structure for inspector
-- ----------------------------
DROP TABLE IF EXISTS `inspector`;
CREATE TABLE `inspector`  (
  `I_ID` int(0) NOT NULL,
  `I_Name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `I_No` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `I_Gender` varchar(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `I_Age` int(0) NOT NULL,
  `I_Tel` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `I_Login_ID` int(0) NOT NULL,
  PRIMARY KEY (`I_ID`) USING BTREE,
  INDEX `I_Login_ID`(`I_Login_ID`) USING BTREE,
  CONSTRAINT `inspector_ibfk_1` FOREIGN KEY (`I_Login_ID`) REFERENCES `login_account` (`L_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for inspector_competition
-- ----------------------------
DROP TABLE IF EXISTS `inspector_competition`;
CREATE TABLE `inspector_competition`  (
  `IC_ID` int(0) NOT NULL AUTO_INCREMENT COMMENT '自增长ID',
  `I_ID` int(0) NOT NULL COMMENT '检录ID\r\n',
  `CI_ID` int(0) NOT NULL COMMENT '比赛对应项目ID\r\n',
  PRIMARY KEY (`IC_ID`) USING BTREE,
  INDEX `I_ID`(`I_ID`) USING BTREE,
  INDEX `CI_ID`(`CI_ID`) USING BTREE,
  CONSTRAINT `inspector_competition_ibfk_1` FOREIGN KEY (`I_ID`) REFERENCES `inspector` (`I_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `inspector_competition_ibfk_2` FOREIGN KEY (`CI_ID`) REFERENCES `competitioninformation` (`CI_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for leader
-- ----------------------------
DROP TABLE IF EXISTS `leader`;
CREATE TABLE `leader`  (
  `L_ID` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `L_Name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `L_No` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `L_Gender` varchar(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `L_Birth` date NOT NULL,
  `L_Tel` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `L_College_ID` int(0) NOT NULL,
  `L_Login_ID` int(0) NOT NULL,
  PRIMARY KEY (`L_ID`) USING BTREE,
  INDEX `L_College_ID`(`L_College_ID`) USING BTREE,
  INDEX `L_Login_ID`(`L_Login_ID`) USING BTREE,
  CONSTRAINT `leader_ibfk_1` FOREIGN KEY (`L_College_ID`) REFERENCES `college` (`C_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `leader_ibfk_2` FOREIGN KEY (`L_Login_ID`) REFERENCES `login_account` (`L_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of leader
-- ----------------------------
INSERT INTO `leader` VALUES ('1', '软件', '72562', '男', '2000-01-23', '13698564123', 15, 8);

-- ----------------------------
-- Table structure for login_account
-- ----------------------------
DROP TABLE IF EXISTS `login_account`;
CREATE TABLE `login_account`  (
  `L_ID` int(0) NOT NULL AUTO_INCREMENT COMMENT '自动编号，登录ID',
  `QXtype` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '角色权限类型',
  `Account` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户名（昵称）',
  `Password` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '密码',
  PRIMARY KEY (`L_ID`) USING BTREE,
  UNIQUE INDEX `Account`(`Account`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of login_account
-- ----------------------------
INSERT INTO `login_account` VALUES (1, '1', '21206112lcw', '1234');
INSERT INTO `login_account` VALUES (2, '1', '21206110kkl', '123');
INSERT INTO `login_account` VALUES (3, '1', '21206109klj', '123');
INSERT INTO `login_account` VALUES (4, '1', '21012515pb', '123');
INSERT INTO `login_account` VALUES (5, '5', '50001', '123');
INSERT INTO `login_account` VALUES (6, '5', '50002', '123');
INSERT INTO `login_account` VALUES (7, '4', '40001', '123');
INSERT INTO `login_account` VALUES (8, '2', '20015', '123');
INSERT INTO `login_account` VALUES (9, '4', '40002', '123');
INSERT INTO `login_account` VALUES (10, '4', '40003', '123');

-- ----------------------------
-- Table structure for preliminaryinformation
-- ----------------------------
DROP TABLE IF EXISTS `preliminaryinformation`;
CREATE TABLE `preliminaryinformation`  (
  `PI_ID` int(0) NOT NULL AUTO_INCREMENT COMMENT '初赛信息ID',
  `CI_ID` int(0) NOT NULL COMMENT '比赛ID',
  `A_ID` int(0) NOT NULL COMMENT '运动员ID',
  `R_ID` int(0) NULL DEFAULT NULL COMMENT '裁判长ID',
  `I_ID` int(0) NULL DEFAULT NULL COMMENT '检录员ID',
  `PI_RecordingStatus` int(0) UNSIGNED NOT NULL DEFAULT 0 COMMENT '检录状态(0为未检录，1为已检录)',
  `PI_RecordingTime` datetime(0) NULL DEFAULT NULL COMMENT '检录时间',
  `PI_Group` int(0) NULL DEFAULT NULL COMMENT '组次',
  `PI_Runway` int(0) NULL DEFAULT NULL COMMENT '跑道',
  `PI_IsFinally` int(0) NULL DEFAULT NULL COMMENT '是否进入决赛',
  `PI_Grade` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '初赛成绩',
  PRIMARY KEY (`PI_ID`) USING BTREE,
  INDEX `CI_ID`(`CI_ID`) USING BTREE,
  INDEX `A_ID`(`A_ID`) USING BTREE,
  INDEX `R_ID`(`R_ID`) USING BTREE,
  INDEX `I_ID`(`I_ID`) USING BTREE,
  CONSTRAINT `preliminaryinformation_ibfk_1` FOREIGN KEY (`CI_ID`) REFERENCES `competitioninformation` (`CI_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `preliminaryinformation_ibfk_2` FOREIGN KEY (`A_ID`) REFERENCES `athlete` (`A_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `preliminaryinformation_ibfk_3` FOREIGN KEY (`R_ID`) REFERENCES `refereechief` (`R_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `preliminaryinformation_ibfk_4` FOREIGN KEY (`I_ID`) REFERENCES `inspector` (`I_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of preliminaryinformation
-- ----------------------------
INSERT INTO `preliminaryinformation` VALUES (1, 3, 1, 1, NULL, 1, '2023-06-02 01:11:19', 1, 1, NULL, '11.7S');
INSERT INTO `preliminaryinformation` VALUES (2, 3, 2, NULL, NULL, 0, NULL, 1, 2, NULL, NULL);
INSERT INTO `preliminaryinformation` VALUES (3, 6, 1, NULL, NULL, 0, NULL, 1, 1, NULL, NULL);

-- ----------------------------
-- Table structure for referee_competition
-- ----------------------------
DROP TABLE IF EXISTS `referee_competition`;
CREATE TABLE `referee_competition`  (
  `RC_ID` int(0) NOT NULL AUTO_INCREMENT COMMENT '裁判-项目ID,自增长ID',
  `R_ID` int(0) NOT NULL COMMENT '裁判长ID',
  `CI_ID` int(0) NOT NULL COMMENT '比赛对应项目ID',
  PRIMARY KEY (`RC_ID`) USING BTREE,
  INDEX `R_ID`(`R_ID`) USING BTREE,
  INDEX `CI_ID`(`CI_ID`) USING BTREE,
  CONSTRAINT `referee_competition_ibfk_1` FOREIGN KEY (`R_ID`) REFERENCES `refereechief` (`R_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `referee_competition_ibfk_2` FOREIGN KEY (`CI_ID`) REFERENCES `competitioninformation` (`CI_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of referee_competition
-- ----------------------------
INSERT INTO `referee_competition` VALUES (3, 1, 3);
INSERT INTO `referee_competition` VALUES (4, 1, 5);
INSERT INTO `referee_competition` VALUES (5, 1, 6);

-- ----------------------------
-- Table structure for refereechief
-- ----------------------------
DROP TABLE IF EXISTS `refereechief`;
CREATE TABLE `refereechief`  (
  `R_ID` int(0) NOT NULL,
  `R_Name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `R_No` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `R_Gender` varchar(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `R_Age` int(0) NOT NULL,
  `R_Tel` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `R_Login_ID` int(0) NOT NULL,
  PRIMARY KEY (`R_ID`) USING BTREE,
  INDEX `R_Login_ID`(`R_Login_ID`) USING BTREE,
  CONSTRAINT `refereechief_ibfk_1` FOREIGN KEY (`R_Login_ID`) REFERENCES `login_account` (`L_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of refereechief
-- ----------------------------
INSERT INTO `refereechief` VALUES (1, '叶溪', '75615', '男', 36, '13648926523', 7);
INSERT INTO `refereechief` VALUES (2, '张韵茵', '74586', '女', 34, '18060560743', 8);
INSERT INTO `refereechief` VALUES (3, '李天宇', '74808', '男', 26, '15967965067', 9);

-- ----------------------------
-- Table structure for signuprecord
-- ----------------------------
DROP TABLE IF EXISTS `signuprecord`;
CREATE TABLE `signuprecord`  (
  `SR_ID` int(0) NOT NULL AUTO_INCREMENT COMMENT '报名ID',
  `SI_ID` int(0) NOT NULL COMMENT '报名项目ID',
  `A_ID` int(0) NOT NULL COMMENT '运动员ID',
  `SR_BestGrade` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '项目最好成绩',
  `SR_Time` datetime(0) NOT NULL COMMENT '项目报名时间',
  `SR_Status` int(0) NOT NULL DEFAULT 0 COMMENT '项目审核状态 0为待审核，1为未通过，2为已通过 默认值为0',
  `SR_Audit_Time` datetime(0) NULL DEFAULT NULL COMMENT '项目审核时间',
  PRIMARY KEY (`SR_ID`) USING BTREE,
  INDEX `SI_ID`(`SI_ID`) USING BTREE,
  INDEX `signuprecord_ibfk_2`(`A_ID`) USING BTREE,
  CONSTRAINT `signuprecord_ibfk_1` FOREIGN KEY (`SI_ID`) REFERENCES `sportitem` (`SI_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `signuprecord_ibfk_2` FOREIGN KEY (`A_ID`) REFERENCES `athlete` (`A_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 137 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of signuprecord
-- ----------------------------
INSERT INTO `signuprecord` VALUES (122, 1, 1, '11.3秒', '2023-05-24 20:57:40', 2, '2023-06-29 19:58:53');
INSERT INTO `signuprecord` VALUES (123, 1, 2, '11.4秒', '2023-05-24 20:57:45', 2, '2023-05-25 20:56:54');
INSERT INTO `signuprecord` VALUES (125, 3, 1, '52.8秒', '2023-05-24 22:01:08', 2, '2023-05-29 21:17:28');
INSERT INTO `signuprecord` VALUES (126, 1, 4, '13.6秒', '2023-05-25 01:28:59', 0, NULL);
INSERT INTO `signuprecord` VALUES (128, 4, 3, '15分钟12秒', '2023-05-26 16:55:17', 0, NULL);
INSERT INTO `signuprecord` VALUES (131, 7, 1, '25分20秒', '2023-05-30 20:22:45', 1, '2023-06-29 19:59:06');
INSERT INTO `signuprecord` VALUES (133, 6, 1, '15分12秒', '2023-05-30 20:24:47', 0, NULL);

-- ----------------------------
-- Table structure for sportitem
-- ----------------------------
DROP TABLE IF EXISTS `sportitem`;
CREATE TABLE `sportitem`  (
  `SI_ID` int(0) NOT NULL AUTO_INCREMENT COMMENT '运动会项目编号',
  `SI_Name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '运动会项目名字',
  `SI_Category` int(0) NOT NULL COMMENT '运动会项目属性（个人赛为0，团体赛为1）',
  `SI_IsFinals` int(0) NOT NULL COMMENT '运动会项目是否预决赛（需要为0，不需要为1）',
  `SI_Introduction` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '运动会项目文字描述\r\n',
  PRIMARY KEY (`SI_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sportitem
-- ----------------------------
INSERT INTO `sportitem` VALUES (1, '100米', 0, 0, '短跑比赛');
INSERT INTO `sportitem` VALUES (2, '200米', 0, 0, '');
INSERT INTO `sportitem` VALUES (3, '400米', 0, 0, '');
INSERT INTO `sportitem` VALUES (4, '800米', 0, 0, NULL);
INSERT INTO `sportitem` VALUES (5, '1500米', 0, 0, '');
INSERT INTO `sportitem` VALUES (6, '3000米', 0, 0, '');
INSERT INTO `sportitem` VALUES (7, '5000米', 0, 0, '');
INSERT INTO `sportitem` VALUES (8, '4╳100米接力', 1, 1, '接力赛');
INSERT INTO `sportitem` VALUES (9, '4╳400米接力', 1, 1, '');
INSERT INTO `sportitem` VALUES (10, '跳远', 0, 0, NULL);
INSERT INTO `sportitem` VALUES (11, '跳高', 0, 0, NULL);
INSERT INTO `sportitem` VALUES (12, '三级跳远', 0, 0, NULL);
INSERT INTO `sportitem` VALUES (13, '铅球', 0, 0, NULL);
INSERT INTO `sportitem` VALUES (14, '袋鼠跳', 0, 0, NULL);
INSERT INTO `sportitem` VALUES (15, '双脚跳绳接力赛', 0, 0, NULL);
INSERT INTO `sportitem` VALUES (16, '夹球跑接力赛', 0, 0, NULL);

-- ----------------------------
-- Table structure for systemadmin
-- ----------------------------
DROP TABLE IF EXISTS `systemadmin`;
CREATE TABLE `systemadmin`  (
  `S_ID` int(0) NOT NULL AUTO_INCREMENT COMMENT '自增长，系统管理员编号',
  `S_Name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '系统管理员姓名',
  `S_Tel` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '系统管理员联系方式',
  `S_Login_ID` int(0) NOT NULL COMMENT '系统管理员登录ID字段',
  PRIMARY KEY (`S_ID`) USING BTREE,
  INDEX `S_Login_ID`(`S_Login_ID`) USING BTREE,
  CONSTRAINT `systemadmin_ibfk_1` FOREIGN KEY (`S_Login_ID`) REFERENCES `login_account` (`L_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- View structure for v_athlete_information
-- ----------------------------
DROP VIEW IF EXISTS `v_athlete_information`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_athlete_information` AS select `login_account`.`Account` AS `Account`,`athlete`.`A_ID` AS `A_ID`,`athlete`.`A_No` AS `A_No`,`athlete`.`A_Name` AS `A_Name`,`athlete`.`A_Gender` AS `A_Gender`,`athlete`.`A_Birth` AS `A_Birth`,`athlete`.`A_Tel` AS `A_Tel`,`college`.`C_NAME` AS `College`,`groupz`.`G_NAME` AS `Group` from (((`athlete` join `college` on((`athlete`.`A_College_ID` = `college`.`C_ID`))) join `groupz` on((`athlete`.`A_Group_ID` = `groupz`.`G_ID`))) join `login_account` on((`athlete`.`A_Login_ID` = `login_account`.`L_ID`)));

-- ----------------------------
-- View structure for v_athlete_signuprecords
-- ----------------------------
DROP VIEW IF EXISTS `v_athlete_signuprecords`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_athlete_signuprecords` AS select `signuprecord`.`SR_ID` AS `SR_ID`,`signuprecord`.`A_ID` AS `A_ID`,`athlete`.`A_Name` AS `A_Name`,`athlete`.`A_No` AS `A_No`,`athlete`.`A_Gender` AS `A_Gender`,`groupz`.`G_NAME` AS `G_NAME`,`athlete`.`A_College_ID` AS `A_College_ID`,`college`.`C_NAME` AS `COLLEGE_NAME`,`signuprecord`.`SI_ID` AS `SI_ID`,`sportitem`.`SI_Name` AS `SI_Name`,`signuprecord`.`SR_BestGrade` AS `SR_BestGrade`,`signuprecord`.`SR_Time` AS `SR_Time`,`signuprecord`.`SR_Status` AS `SR_Status`,`signuprecord`.`SR_Audit_Time` AS `SR_Audit_Time` from ((((`signuprecord` join `athlete` on((`signuprecord`.`A_ID` = `athlete`.`A_ID`))) join `sportitem` on((`signuprecord`.`SI_ID` = `sportitem`.`SI_ID`))) join `groupz` on((`athlete`.`A_Group_ID` = `groupz`.`G_ID`))) join `college` on((`athlete`.`A_College_ID` = `college`.`C_ID`)));

-- ----------------------------
-- View structure for v_athleteleader_information
-- ----------------------------
DROP VIEW IF EXISTS `v_athleteleader_information`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_athleteleader_information` AS select `login_account`.`Account` AS `Account`,`leader`.`L_ID` AS `L_ID`,`leader`.`L_Name` AS `L_Name`,`leader`.`L_No` AS `L_No`,`leader`.`L_Gender` AS `L_Gender`,`leader`.`L_Birth` AS `L_Birth`,`leader`.`L_Tel` AS `L_Tel`,`college`.`C_NAME` AS `college`,`college`.`C_ID` AS `C_ID` from ((`leader` join `login_account` on((`leader`.`L_Login_ID` = `login_account`.`L_ID`))) join `college` on((`leader`.`L_College_ID` = `college`.`C_ID`)));

-- ----------------------------
-- View structure for v_competition_information
-- ----------------------------
DROP VIEW IF EXISTS `v_competition_information`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_competition_information` AS select `competitioninformation`.`CI_ID` AS `CI_ID`,`competitioninformation`.`CI_Venue` AS `CI_Venue`,`competitioninformation`.`CI_TIME` AS `CI_TIME`,`competitioninformation`.`SI_ID` AS `SI_ID`,`sportitem`.`SI_Name` AS `SI_Name`,`sportitem`.`SI_Category` AS `SI_Category`,`competitioninformation`.`CI_Limit` AS `CI_Limit`,`competitioninformation`.`CI_Nature` AS `CI_Nature`,`competitioninformation`.`G_ID` AS `G_ID`,`groupz`.`G_NAME` AS `G_NAME` from ((`competitioninformation` join `sportitem` on((`competitioninformation`.`SI_ID` = `sportitem`.`SI_ID`))) join `groupz` on((`competitioninformation`.`G_ID` = `groupz`.`G_ID`)));

-- ----------------------------
-- View structure for v_finalsinformation_athlete
-- ----------------------------
DROP VIEW IF EXISTS `v_finalsinformation_athlete`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_finalsinformation_athlete` AS select `finalsinformation`.`FI_ID` AS `ID`,`sportitem`.`SI_Name` AS `SI_Name`,`groupz`.`G_NAME` AS `G_NAME`,`competitioninformation`.`CI_TIME` AS `CI_TIME`,`competitioninformation`.`CI_Issue` AS `CI_Issue`,`finalsinformation`.`A_ID` AS `A_ID`,`athlete`.`A_Name` AS `A_Name`,`athlete`.`A_No` AS `A_No`,`athlete`.`A_Gender` AS `A_Gender`,`finalsinformation`.`FI_RecordingStatus` AS `RecordingStatus`,`finalsinformation`.`FI_Group` AS `Group`,`finalsinformation`.`FI_Runway` AS `Runway`,`finalsinformation`.`FI_Grade` AS `Grade` from ((((`finalsinformation` join `competitioninformation` on((`finalsinformation`.`CI_ID` = `competitioninformation`.`CI_ID`))) join `athlete` on((`finalsinformation`.`A_ID` = `athlete`.`A_ID`))) join `sportitem` on((`competitioninformation`.`SI_ID` = `sportitem`.`SI_ID`))) join `groupz` on(((`athlete`.`A_Group_ID` = `groupz`.`G_ID`) and (`competitioninformation`.`G_ID` = `groupz`.`G_ID`))));

-- ----------------------------
-- View structure for v_finalsinformation_referee
-- ----------------------------
DROP VIEW IF EXISTS `v_finalsinformation_referee`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_finalsinformation_referee` AS select `finalsinformation`.`FI_ID` AS `PIorFI_ID`,`finalsinformation`.`A_ID` AS `A_ID`,`athlete`.`A_Name` AS `A_Name`,`athlete`.`A_No` AS `A_No`,`athlete`.`A_Gender` AS `A_Gender`,`athlete`.`A_College_ID` AS `A_College_ID`,`college`.`C_NAME` AS `A_College_Name`,`athlete`.`A_Tel` AS `A_Tel`,`athlete`.`A_Group_ID` AS `A_Group_ID`,`groupz`.`G_NAME` AS `A_Group_Name`,`finalsinformation`.`CI_ID` AS `CI_ID`,`finalsinformation`.`R_ID` AS `R_ID`,`finalsinformation`.`I_ID` AS `I_ID`,`finalsinformation`.`FI_RecordingStatus` AS `PIorFI_RecordingStatus`,`finalsinformation`.`FI_RecordingTime` AS `PIorFI_RecordingTime`,`finalsinformation`.`FI_Group` AS `PIorFI_Group`,`finalsinformation`.`FI_Runway` AS `PIorFI_Runway`,`finalsinformation`.`FI_Grade` AS `PIorFI_Grade` from (((`groupz` join `athlete` on((`groupz`.`G_ID` = `athlete`.`A_Group_ID`))) join `college` on((`athlete`.`A_College_ID` = `college`.`C_ID`))) join `finalsinformation` on((`athlete`.`A_ID` = `finalsinformation`.`A_ID`)));

-- ----------------------------
-- View structure for v_headreferee_information
-- ----------------------------
DROP VIEW IF EXISTS `v_headreferee_information`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_headreferee_information` AS select `login_account`.`Account` AS `Account`,`headreferee`.`H_ID` AS `H_ID`,`headreferee`.`H_No` AS `H_No`,`headreferee`.`H_Name` AS `H_Name`,`headreferee`.`H_Gender` AS `H_Gender`,`headreferee`.`H_Age` AS `H_Age`,`headreferee`.`H_Tel` AS `H_Tel` from (`headreferee` join `login_account` on((`headreferee`.`H_Login_ID` = `login_account`.`L_ID`)));

-- ----------------------------
-- View structure for v_preliminaryinformation_athlete
-- ----------------------------
DROP VIEW IF EXISTS `v_preliminaryinformation_athlete`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_preliminaryinformation_athlete` AS select `preliminaryinformation`.`PI_ID` AS `ID`,`sportitem`.`SI_Name` AS `SI_Name`,`groupz`.`G_NAME` AS `G_NAME`,`competitioninformation`.`CI_TIME` AS `CI_TIME`,`competitioninformation`.`CI_Issue` AS `CI_Issue`,`athlete`.`A_ID` AS `A_ID`,`athlete`.`A_Name` AS `A_Name`,`athlete`.`A_No` AS `A_No`,`athlete`.`A_Gender` AS `A_Gender`,`preliminaryinformation`.`PI_RecordingStatus` AS `RecordingStatus`,`preliminaryinformation`.`PI_Group` AS `Group`,`preliminaryinformation`.`PI_Runway` AS `Runway`,`preliminaryinformation`.`PI_Grade` AS `Grade` from ((((`preliminaryinformation` join `competitioninformation` on((`preliminaryinformation`.`CI_ID` = `competitioninformation`.`CI_ID`))) join `sportitem` on((`competitioninformation`.`SI_ID` = `sportitem`.`SI_ID`))) join `groupz` on((`competitioninformation`.`G_ID` = `groupz`.`G_ID`))) join `athlete` on(((`groupz`.`G_ID` = `athlete`.`A_Group_ID`) and (`preliminaryinformation`.`A_ID` = `athlete`.`A_ID`))));

-- ----------------------------
-- View structure for v_preliminaryinformation_referee
-- ----------------------------
DROP VIEW IF EXISTS `v_preliminaryinformation_referee`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_preliminaryinformation_referee` AS select `preliminaryinformation`.`PI_ID` AS `PIorFI_ID`,`preliminaryinformation`.`A_ID` AS `A_ID`,`athlete`.`A_Name` AS `A_Name`,`athlete`.`A_No` AS `A_No`,`athlete`.`A_Gender` AS `A_Gender`,`athlete`.`A_College_ID` AS `A_College_ID`,`college`.`C_NAME` AS `A_College_Name`,`athlete`.`A_Tel` AS `A_Tel`,`athlete`.`A_Group_ID` AS `A_Group_ID`,`groupz`.`G_NAME` AS `A_Group_Name`,`preliminaryinformation`.`CI_ID` AS `CI_ID`,`preliminaryinformation`.`R_ID` AS `R_ID`,`preliminaryinformation`.`I_ID` AS `I_ID`,`preliminaryinformation`.`PI_RecordingStatus` AS `PIorFI_RecordingStatus`,`preliminaryinformation`.`PI_RecordingTime` AS `PIorFI_RecordingTime`,`preliminaryinformation`.`PI_Group` AS `PIorFI_Group`,`preliminaryinformation`.`PI_Runway` AS `PIorFI_Runway`,`preliminaryinformation`.`PI_Grade` AS `PIorFI_Grade` from (((`groupz` join `athlete` on((`groupz`.`G_ID` = `athlete`.`A_Group_ID`))) join `preliminaryinformation` on((`preliminaryinformation`.`A_ID` = `athlete`.`A_ID`))) join `college` on((`athlete`.`A_College_ID` = `college`.`C_ID`)));

-- ----------------------------
-- View structure for v_referee_information
-- ----------------------------
DROP VIEW IF EXISTS `v_referee_information`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_referee_information` AS select `login_account`.`Account` AS `Account`,`refereechief`.`R_ID` AS `R_ID`,`refereechief`.`R_No` AS `R_No`,`refereechief`.`R_Name` AS `R_Name`,`refereechief`.`R_Gender` AS `R_Gender`,`refereechief`.`R_Age` AS `R_Age`,`refereechief`.`R_Tel` AS `R_Tel` from (`refereechief` join `login_account` on((`refereechief`.`R_Login_ID` = `login_account`.`L_ID`)));

-- ----------------------------
-- View structure for v_referee_item_information
-- ----------------------------
DROP VIEW IF EXISTS `v_referee_item_information`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_referee_item_information` AS select `referee_competition`.`RC_ID` AS `RC_ID`,`referee_competition`.`R_ID` AS `R_ID`,`refereechief`.`R_Name` AS `R_Name`,`referee_competition`.`CI_ID` AS `CI_ID`,`competitioninformation`.`CI_Venue` AS `CI_Venue`,`competitioninformation`.`CI_TIME` AS `CI_TIME`,`competitioninformation`.`CI_Issue` AS `CI_Issue`,`sportitem`.`SI_ID` AS `SI_ID`,`sportitem`.`SI_Name` AS `SI_Name`,`sportitem`.`SI_Introduction` AS `SI_Introduction`,`groupz`.`G_ID` AS `G_ID`,`groupz`.`G_NAME` AS `G_NAME`,(case `competitioninformation`.`CI_Nature` when 1 then '预赛' when 2 then '决赛' when 3 then '预决赛' else `competitioninformation`.`CI_Nature` end) AS `CI_Nature` from ((((`referee_competition` join `refereechief` on((`referee_competition`.`R_ID` = `refereechief`.`R_ID`))) join `competitioninformation` on((`referee_competition`.`CI_ID` = `competitioninformation`.`CI_ID`))) join `sportitem` on((`competitioninformation`.`SI_ID` = `sportitem`.`SI_ID`))) join `groupz` on((`competitioninformation`.`G_ID` = `groupz`.`G_ID`)));

-- ----------------------------
-- Procedure structure for C_P_Audit_NotApproved_SignUpRecord
-- ----------------------------
DROP PROCEDURE IF EXISTS `C_P_Audit_NotApproved_SignUpRecord`;
delimiter ;;
CREATE PROCEDURE `C_P_Audit_NotApproved_SignUpRecord`(IN `ssr_id` INT)
BEGIN
    UPDATE signuprecord SET SR_Status = 1, SR_Audit_Time = NOW() WHERE SR_ID = ssr_id;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for C_P_Audit_Quash_SignUpRecord
-- ----------------------------
DROP PROCEDURE IF EXISTS `C_P_Audit_Quash_SignUpRecord`;
delimiter ;;
CREATE PROCEDURE `C_P_Audit_Quash_SignUpRecord`(IN `ssr_id` INT)
BEGIN
    UPDATE signuprecord SET SR_Status = 0, SR_Audit_Time = NULL WHERE SR_ID = ssr_id;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for C_P_Audit_SignUpRecord
-- ----------------------------
DROP PROCEDURE IF EXISTS `C_P_Audit_SignUpRecord`;
delimiter ;;
CREATE PROCEDURE `C_P_Audit_SignUpRecord`(IN `ssr_id` INT)
BEGIN
    DECLARE si_count INT;
		DECLARE athleteItemCount INT;
    
    -- 获取SR_Status、SI_ID和A_College_ID
    SELECT SR_Status, SI_ID, A_College_ID,athlete.A_ID INTO @sr_status, @si_id, @college_id, @a_id FROM signuprecord JOIN athlete ON signuprecord.A_ID = athlete.A_ID WHERE signuprecord.SR_ID = ssr_id;
    
    -- 计算同一个SI_ID且同一个C_ID的数量
    SELECT COUNT(*) INTO si_count FROM signuprecord JOIN athlete ON signuprecord.A_ID = athlete.A_ID WHERE athlete.A_College_ID = @college_id AND signuprecord.SI_ID = @si_id AND signuprecord.SR_Status = 2;
		
		-- 计算同一个运动员报名的项目数
		SELECT COUNT(*) INTO athleteItemCount FROM signuprecord JOIN athlete ON signuprecord.A_ID = athlete.A_ID WHERE signuprecord.A_ID = @a_id AND signuprecord.SR_Status = 2;
    
    -- 如果同一个SI_ID且同一个C_ID的数量等于2，则报错；否则更新SR_Status为2
    IF si_count >= 2 THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = '该项目人数达到上限，无法继续通过审核';
		ELSEIF athleteItemCount >= 2 THEN
				SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = '该运动员报名已达上限，无法继续通过审核';
    ELSE
        UPDATE signuprecord SET SR_Status = 2, SR_Audit_Time = NOW() WHERE SR_ID = ssr_id;
    END IF;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for C_P_DeleteItemAndRecord
-- ----------------------------
DROP PROCEDURE IF EXISTS `C_P_DeleteItemAndRecord`;
delimiter ;;
CREATE PROCEDURE `C_P_DeleteItemAndRecord`(IN ID INT)
BEGIN
    
    DELETE FROM PreliminaryInformation WHERE CI_ID IN (SELECT CI_ID FROM CompetitionInformation WHERE SI_ID = ID);
    DELETE FROM FinalsInformation WHERE CI_ID IN (SELECT CI_ID FROM CompetitionInformation WHERE SI_ID = ID);

    -- Delete related records in Referee_Competition table and Inspector_Competition table
    DELETE FROM Referee_Competition WHERE CI_ID IN (SELECT CI_ID FROM CompetitionInformation WHERE SI_ID = ID);
    DELETE FROM Inspector_Competition WHERE CI_ID IN (SELECT CI_ID FROM CompetitionInformation WHERE SI_ID = ID);
		
		DELETE FROM competitioninformation WHERE SI_ID = ID;
		DELETE FROM signuprecord WHERE SI_ID = ID;

    -- Delete the sport item record
    DELETE FROM SportItem WHERE SI_ID = ID;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for C_P_INSERT_CompetitionInformation
-- ----------------------------
DROP PROCEDURE IF EXISTS `C_P_INSERT_CompetitionInformation`;
delimiter ;;
CREATE PROCEDURE `C_P_INSERT_CompetitionInformation`(IN inCI_Venue VARCHAR(50),
    IN inCI_TIME DATETIME,
    IN inS_ID INT,
    IN inCI_Limit INT,
    IN inCI_Nature INT,
    IN inG_ID INT)
BEGIN
    -- 判断是否已经存在相同的 S_ID、G_ID 和 CI_Nature，如果存在则返回错误
    IF EXISTS(SELECT CI_ID FROM competitioninformation WHERE SI_ID = inS_ID AND G_ID = inG_ID AND CI_Nature = inCI_Nature) THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = '存在相同项目记录，不允许插入';
    ELSE
        -- 如果不存在，则插入新记录
        INSERT INTO competitioninformation(CI_Venue, CI_TIME, SI_ID, CI_Limit, CI_Nature, G_ID)
        VALUES (inCI_Venue, inCI_TIME, inS_ID, inCI_Limit, inCI_Nature, inG_ID);
        
    END IF;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for C_P_Search_AthleteCompetitionInformation
-- ----------------------------
DROP PROCEDURE IF EXISTS `C_P_Search_AthleteCompetitionInformation`;
delimiter ;;
CREATE PROCEDURE `C_P_Search_AthleteCompetitionInformation`(IN ici_id INT)
BEGIN
    DECLARE nature INT;
    
    SELECT CI_Nature INTO nature FROM competitioninformation WHERE CI_ID = ici_id;
    
    IF (nature = 1) THEN
        SELECT * FROM v_preliminaryinformation_referee WHERE CI_ID = ici_id ORDER BY PIorFI_Group ASC, PIorFI_Runway ASC;
    ELSEIF (nature = 2 OR nature = 3) THEN
        SELECT * FROM v_finalsinformation_referee WHERE CI_ID = ici_id ORDER BY PIorFI_Group ASC, PIorFI_Runway ASC;
    END IF;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for C_P_Search_Grade_Athlete
-- ----------------------------
DROP PROCEDURE IF EXISTS `C_P_Search_Grade_Athlete`;
delimiter ;;
CREATE PROCEDURE `C_P_Search_Grade_Athlete`(IN iA_id INT)
BEGIN
    -- 查询 Preliminary 信息
    SELECT 
        PI_ID AS ID,
        SI_Name,
        G_NAME,
        CI_TIME,
        CI_Issue,
        athlete.A_ID AS A_ID,
        A_Name,
        A_No,
        A_Gender,
        PI_RecordingStatus AS RecordingStatus,
        PI_Group AS `Group`,
        PI_Runway AS Runway,
        PI_Grade AS Grade
    FROM (((preliminaryinformation 
    JOIN competitioninformation ON preliminaryinformation.CI_ID = competitioninformation.CI_ID) 
    JOIN sportitem ON competitioninformation.SI_ID = sportitem.SI_ID)
    JOIN groupz ON competitioninformation.G_ID = groupz.G_ID)
    JOIN athlete ON groupz.G_ID = athlete.A_Group_ID AND preliminaryinformation.A_ID = athlete.A_ID
    WHERE athlete.A_ID = iA_id AND CI_Issue = 1
    
    UNION ALL 
    
    -- 查询 Finals 信息
    SELECT 
        FI_ID AS ID,
        SI_Name,
        G_NAME,
        CI_TIME,
        CI_Issue,
        athlete.A_ID AS A_ID,
        A_Name,
        A_No,
        A_Gender,
        FI_RecordingStatus AS RecordingStatus,
        FI_Group AS `Group`,
        FI_Runway AS Runway,
        FI_Grade AS Grade
    FROM (((finalsinformation 
    JOIN competitioninformation ON finalsinformation.CI_ID = competitioninformation.CI_ID) 
    JOIN athlete ON finalsinformation.A_ID = athlete.A_ID)
    JOIN sportitem ON competitioninformation.SI_ID = sportitem.SI_ID)
    JOIN groupz ON athlete.A_Group_ID = groupz.G_ID AND competitioninformation.G_ID = groupz.G_ID
    WHERE athlete.A_ID = iA_id AND CI_Issue = 1;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for C_P_Search_Grade_College
-- ----------------------------
DROP PROCEDURE IF EXISTS `C_P_Search_Grade_College`;
delimiter ;;
CREATE PROCEDURE `C_P_Search_Grade_College`(IN iC_id INT)
BEGIN
    -- 查询 Preliminary 信息
    SELECT 
        PI_ID AS ID,
        SI_Name,
        G_NAME,
        CI_TIME,
        CI_Issue,
        athlete.A_ID AS A_ID,
        A_Name,
        A_No,
        A_Gender,
        PI_RecordingStatus AS RecordingStatus,
        PI_Group AS `Group`,
        PI_Runway AS Runway,
        PI_Grade AS Grade
    FROM (((preliminaryinformation 
    JOIN competitioninformation ON preliminaryinformation.CI_ID = competitioninformation.CI_ID) 
    JOIN sportitem ON competitioninformation.SI_ID = sportitem.SI_ID)
    JOIN groupz ON competitioninformation.G_ID = groupz.G_ID)
    JOIN athlete ON groupz.G_ID = athlete.A_Group_ID AND preliminaryinformation.A_ID = athlete.A_ID
    WHERE athlete.A_College_ID = iC_id AND CI_Issue = 1
    
    UNION ALL 
    
    -- 查询 Finals 信息
    SELECT 
        FI_ID AS ID,
        SI_Name,
        G_NAME,
        CI_TIME,
        CI_Issue,
        athlete.A_ID AS A_ID,
        A_Name,
        A_No,
        A_Gender,
        FI_RecordingStatus AS RecordingStatus,
        FI_Group AS `Group`,
        FI_Runway AS Runway,
        FI_Grade AS Grade
    FROM (((finalsinformation 
    JOIN competitioninformation ON finalsinformation.CI_ID = competitioninformation.CI_ID) 
    JOIN athlete ON finalsinformation.A_ID = athlete.A_ID)
    JOIN sportitem ON competitioninformation.SI_ID = sportitem.SI_ID)
    JOIN groupz ON athlete.A_Group_ID = groupz.G_ID AND competitioninformation.G_ID = groupz.G_ID
    WHERE athlete.A_College_ID = iC_id AND CI_Issue = 1;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for C_P_UpdateAthleteGrade
-- ----------------------------
DROP PROCEDURE IF EXISTS `C_P_UpdateAthleteGrade`;
delimiter ;;
CREATE PROCEDURE `C_P_UpdateAthleteGrade`(IN ici_id INT, IN id INT, IN score VARCHAR(20),IN irid INT)
BEGIN
    DECLARE Nature INT;
    
    -- 查询competitioninformation表中对应ID的CI_Nature值
    SELECT CI_Nature INTO Nature FROM competitioninformation WHERE CI_ID = ici_id;
    
    -- 如果查询到CI_Nature为1，则更新preliminaryinformation表的PI_GRADE字段
    IF Nature = 1 THEN
        UPDATE preliminaryinformation SET PI_GRADE = score,R_ID = irid WHERE PI_ID = id;
    
    -- 如果查询到CI_Nature为2或者3，则更新finalsinformation表的FI_GRADE字段
    ELSEIF Nature = 2 OR Nature = 3 THEN
        UPDATE finalsinformation SET FI_GRADE = score,R_ID = irid  WHERE FI_ID = id;
    END IF;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for C_P_Update_CompetitionInformation
-- ----------------------------
DROP PROCEDURE IF EXISTS `C_P_Update_CompetitionInformation`;
delimiter ;;
CREATE PROCEDURE `C_P_Update_CompetitionInformation`(IN inCI_ID INT,
		IN inCI_Venue VARCHAR(50),
    IN inCI_TIME DATETIME,
    IN inS_ID INT,
    IN inCI_Limit INT,
    IN inCI_Nature INT,
    IN inG_ID INT)
BEGIN
    -- 判断是否已经存在相同的 S_ID、G_ID 和 CI_Nature，如果存在则返回错误
    IF EXISTS(SELECT CI_ID FROM competitioninformation WHERE SI_ID = inS_ID AND G_ID = inG_ID AND CI_Nature = inCI_Nature AND CI_ID <> inCI_ID) THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = '更新后存在相同项目记录，请在上条记录进行更新操作';
    ELSE
        -- 如果不存在，则插入新记录
        UPDATE competitioninformation SET CI_Venue = inCI_Venue,
				CI_TIME = inCI_TIME,
				CI_Limit = inCI_Limit,
				CI_Nature = inCI_Nature,
				G_ID = inG_ID,
				SI_ID = inS_ID
				WHERE CI_ID = inCI_ID;
        
    END IF;
END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
