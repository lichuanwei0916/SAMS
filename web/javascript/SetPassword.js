let captchaValue = "a";  // 定义一个变量保存验证码值

layui.use(['form', 'layer', 'jquery'], function () {
    var form = layui.form;
    var layer = layui.layer;
    var $ = layui.jquery;

    // 表单验证
    form.verify({
        confirm: function (value, item) {
            if (value !== $('[name="newPassword"]').val()) {
                return '两次输入的新密码不一致';
            }
        }
    });

    // 监听表单提交
    form.on('submit(submit-password-form)', function (data) {

        if (data.field.captcha.length != 4) {
            layer.msg("验证码错误");
            return false;  // 阻止表单提交
        }

        // 发送修改密码请求并处理结果
        $.ajax({
            type: 'POST',
            url: '../../checkPassword', // 后台接口地址
            data: {
                oldPassword: data.field.oldPassword
            },
            success: function (result) {
                if (result.code === 0) { // 验证旧密码成功
                    layer.confirm('确定要修改密码吗？', function (index) {
                        // 修改密码请求
                        $.ajax({
                            type: 'POST',
                            url: '../../changePassword', // 后台接口地址
                            data: {
                                newPassword: data.field.newPassword
                            },
                            success: function (result) {
                                if (result.code === 0) { // 修改密码成功
                                    layer.msg('修改密码成功');
                                } else {
                                    layer.msg(result.msg || '修改密码失败');
                                }
                            }
                        });
                        layer.close(index);
                    });
                } else { // 验证旧密码失败
                    layer.msg(result.msg || '旧密码错误');
                }
            }
        });

        return false; // 阻止表单跳转
    });
});
