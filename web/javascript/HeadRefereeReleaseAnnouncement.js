const {createEditor, createToolbar} = window.wangEditor

const editorConfig = {
    placeholder: '请在此处输入要输入的相关信息',
    // onChange(editor) {
    //     const html = editor.getHtml()
    //     console.log('editor content', html)
    //     // 也可以同步到 <textarea>
    // }
}

const editor = createEditor({
    selector: '#editor-container',
    html: '',
    config: editorConfig,
    mode: 'default', // or 'simple'
})

const toolbarConfig = {}

const toolbar = createToolbar({
    editor,
    selector: '#toolbar-container',
    config: toolbarConfig,
    mode: 'default', // or 'simple'
})

layui.use(['layer'], function(){
    var layer = layui.layer;

    // 点击按钮的事件处理
    $('#view').click(function(){
        var content = editor.getHtml(); // 获取编辑器内容
        var cssContent = "<link href=\"../../css/editor.css\" rel=\"stylesheet\">";

        // 弹出窗口
        layer.open({
            title: '公告预览',
            type: 1,
            maxmin: true,
            area: ['70%', '80%'],
            content: cssContent + "<div class='editor-content-view'>" + content + "</div>",
            offset: ['50px', '330px'],
            scrollbar:false
        });
    });
    $('#reset').click(function(){
        editor.clear();
    });
});