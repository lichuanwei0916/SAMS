$(window).on("load",function() {
    var url = window.location.href;
    var start = url.indexOf('Athlete/') + 'Athlete/'.length;
    var end = url.indexOf('.jsp');
    var result = url.substring(start, end);

    if(result == "signup_items" || result == "searchSchedule" || result == "rules" || result == "search_signup_items"){
        $("#my_items").trigger("click");
    }
    else if(result == "query_grades" || result == "appeal_grades")
        $("#my_grades").trigger("click");
    $("#" + result).css({
        "background-color": "rgba(128, 188, 255, 0.7)",
        "border-right": "2.5px solid #ffffff"
    });
});
