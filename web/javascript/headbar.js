layui.use(['layer'], function(){
    var layer = layui.layer;
    var form = layui.form;

    $('#lock').click(function(){
        var cssContent = "<link href=\"../../css/lock.css\" rel=\"stylesheet\">";
        // 打开锁屏弹出窗口
        layer.open({
            type: 1,
            title: false, // 禁用标题栏
            closeBtn: false, // 禁用默认关闭按钮
            area: ['100%', '100%'],
            scrollbar: false, // 暂时屏蔽浏览器滚动条
            anim: -1, // 禁用弹出动画
            isOutAnim: false, // 禁用关闭动画
            id: 'ID-layer-demo-inst',
            skin: 'class-demo-layer-lockscreen', // className
            content: [cssContent,
                '<div class="layui-form">',
                '<div class="layui-input-wrap">',
                '<input type="password" class="class-demo-layer-pin" lay-affix="eye">',
                '<div class="layui-input-suffix">',
                '<i class="layui-icon layui-icon-right" id="ID-layer-demo-unlock"></i>',
                '</div>',
                '</div>',
                '<div>     输入登录密码后回车，即可退出锁屏</div>',
                '</div>'].join(''),
            success: function(layero, index){
                var input = layero.find('input');

                form.render(); // 表单组件渲染
                input.focus();
                // 点击解锁按钮
                var elemUnlock = layero.find('#ID-layer-demo-unlock');
                elemUnlock.on('click', function(){
                    $.ajax({
                        type: 'POST',
                        url: '../../checkPassword', // 后台接口地址
                        data: {
                            oldPassword: $.trim(input[0].value)
                        },
                        success: function (result) {
                            if (result.code === 0) { // 验证密码成功
                                layer.close(index);
                                layer.closeLast('dialog'); // 关闭最新打开的信息框
                            } else { // 验证密码失败
                                layer.msg('登录密码输入有误', {offset: '16px', anim: 'slideDown'})
                                input.focus();
                            }
                        }
                    });
                });
                // 回车
                input.on('keyup', function(e){
                    var elem = this;
                    var keyCode = e.keyCode;
                    if(keyCode === 13){
                        elemUnlock.trigger('click');
                    }
                });
            }
        })
    });
});
