$(window).on("load",function() {
    var url = window.location.href;
    var start = url.indexOf('Referee/') + 'Referee/'.length;
    var end = url.indexOf('.jsp');
    var result = url.substring(start, end);
    if(result == "RefereeProject"){
        $("#sm_items").trigger("click");
    }
    else if(result == "" || result == "")
        $("#my_grades").trigger("click");
    $("#" + result).css({
        "background-color": "rgba(128, 188, 255, 0.7)",
        "border-right": "2.5px solid #ffffff"
    });
});
