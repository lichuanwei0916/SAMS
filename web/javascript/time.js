// 获取时间
window.addEventListener('load', function () {
  // 获取元素
  let time = document.querySelector('.time')// 获取类名为 "date" 的 h6 元素
  //通过 setInterval 方法创建一个间隔计时器
  setInterval(function () {
    // 获取当前时间
    let d = new Date()
    let year = d.getFullYear() // 获取年
    let month = d.getMonth() + 1 // 获取月
    let day = d.getDate() // 获取日期
    let hour = d.getHours() // 获取小时
    let minute = d.getMinutes() // 获取分钟
    let second = d.getSeconds() // 获取秒

    if (month < 10) month = '0' + month
    if (day < 10) day = '0' + day
    if (hour < 10) hour = '0' + hour
    if (minute < 10) minute = '0' + minute
    if (second < 10) second = '0' + second

    // 拼接字符串
    let time_str = year + ' 年 ' + month + ' 月 ' + day + ' 日 <br>' + hour + ' : ' + minute + ' : ' + second

    time.innerHTML = time_str
  }, 1000)
})
