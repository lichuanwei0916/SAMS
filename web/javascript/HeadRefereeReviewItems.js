layui.use(['table', 'form'], function () {
    var table = layui.table;
    var form = layui.form;

    $('.addItem').click(function() {
        $.ajax({
            url: '../../jsp/HeadReferee/addItem.jsp',
            type: 'GET',
            dataType: 'html',
            success: function (content) {
                // 成功获取到页面内容后执行的回调函数
                layer.open({
                    title: '新建项目信息',
                    type: 1,
                    maxmin: true,
                    area: ['50%', '60%'],
                    content: content
                });
            },
            error: function () {
                layer.msg('Failed to load the page');
            }
        });
    });

    $.ajax({
        url: '../../SearchItemInformation',
        method: 'POST',
        dataType: 'json',
        success: function (res) {
            table.render({
                elem: '#dataTable',
                data: res.data,
                toolbar: '#toolbarDemo',//开启工具栏
                cellMinWidth: 100, // 设置单元格最小宽度为100px
                defaultToolbar: ['filter', 'exports', 'print', {
                    title: '提示',
                    layEvent: 'LAYTABLE_TIPS',
                    icon: 'layui-icon-tips'
                }],
                skin:'nob',
                cols: [[
                    {field: 'num', width: 130, title: '项目序号',},
                    {field: 'name', width: 130, title: '项目名称'},
                    {field: 'category', width: 130, title: '项目属性'},
                    {field: 'isFinals', width: 160, title: '项目比赛性质', sort: true},
                    {field: 'introduction', width: 230, title: '项目描述'},
                    {field: 'operation', width: 200, title: '操作', toolbar: '#operationBar'},
                ]],
                page: true
            });
        },
        error: function (err) {
            layer.msg( '查询数据时发生错误，请稍后再试！');
        }
    });

    table.on('tool(dataTable)', function (obj) {
        var data = obj.data; // 获得当前行数据
        var category,isFinals;

        if(data.category === '个人赛')
            category = 0;
        else if(data.category === '团体赛')
            category = 1;

        if(data.isFinals === '预赛和决赛')
            isFinals = 0;
        else if(data.isFinals === '决赛')
            isFinals = 1;
        // console.log(obj)
        if (obj.event === 'edit') {
            $.ajax({
                url: '../../jsp/HeadReferee/updateItem.jsp',
                type: 'GET',
                data: {
                    id: data.id,
                    name: data.name,
                    itemProperty: category,
                    rateProperty: isFinals,
                    text: data.introduction,
                },
                dataType: 'html',
                success: function (content) {
                    // 成功获取到页面内容后执行的回调函数
                    layer.open({
                        title: '编辑项目信息',
                        type: 1,
                        maxmin: true,
                        area: ['50%', '70%'],
                        content: content
                    });
                },
                error: function () {
                    layer.msg('Failed to load the page');
                }
            });
        }
        else if (obj.event === 'delete') {
            $.ajax({
                url: '../../DeleteItemInformation',
                type: 'GET',
                data: {
                    id: data.id,
                },
                success: function (result) {
                    // 成功获取到页面内容后执行的回调函数
                    if (result.code === 200) { // 成功
                        layer.msg('删除成功');
                        $.ajax({
                            url: '../../SearchItemInformation',
                            method: 'POST',
                            dataType: 'json',
                            success: function (res) {
                                table.reload('dataTable',{
                                    page: {
                                        curr: 1 // 重新从第 1 页开始
                                    },
                                    data: res.data
                                });
                            },
                            error: function (err) {
                                layer.msg( '查询数据时发生错误，请稍后再试！');
                            }
                        });
                    } else if(result.code === 100){
                        layer.confirm('该项目有对应的报名记录，确认要删除项目？', {icon: 3}, function(){
                            $.ajax({
                                url: '../../DeleteItemAndRecord',
                                type: 'GET',
                                data: {
                                    id: data.id,
                                },
                                success: function (result) {
                                    // 成功获取到页面内容后执行的回调函数
                                    if (result.code === 200) { // 成功
                                        layer.msg('删除成功');
                                        $.ajax({
                                            url: '../../SearchItemInformation',
                                            method: 'POST',
                                            dataType: 'json',
                                            success: function (res) {
                                                table.reload('dataTable',{
                                                    page: {
                                                        curr: 1 // 重新从第 1 页开始
                                                    },
                                                    data: res.data
                                                });
                                            },
                                            error: function (err) {
                                                layer.msg( '查询数据时发生错误，请稍后再试！');
                                            }
                                        });
                                    }
                                    else
                                        layer.msg('删除失败');
                                },
                                error: function () {
                                    layer.msg('删除失败');
                                }
                            });
                        }, function(){
                            layer.msg('取消删除');
                        });
                    }
                },
                error: function () {
                    layer.msg('删除失败');
                }
            });

        }
    });
    table.on('toolbar(dataTable)', function (obj) {
        var id = obj.config.id;
        var checkStatus = table.checkStatus(id);
        var othis = lay(this);
        switch (obj.event) {
            case 'LAYTABLE_TIPS':
                layer.alert('查看运动会项目详细信息');
                break;
        }
        ;
    });
});
