//获取图片
let img = document.getElementById('picture');
//获取圆点
let span = document.querySelectorAll('span.dot');
//获取左边箭头
let left = document.getElementById('left');
//获取右边箭头
let right = document.getElementById('right');
//初始化当前图片下标
let index = 0;
//timer用于获取自动播放轮播图是设置的定时器的返回值
let timer;
function show(){
  img.src = `../img/img${index}.png`;
}
//设置定时器让轮播图自动播放
function autoPlay() {
  timer = setInterval(function() {
    //设置定时器并接收返回值，便于停止自动播放
    if (index == span.length) {
      index = 0;
    }
    show();
    index++;
  }, 5000);
}
autoPlay();

//设置圆点手动控制切换图片
function ctrlPlay(){
  for(let i = 0; i < span.length; i++){
    //为每个小圆点设置点击事件
    span[i].onclick = function(){
      index = i;
      show();
    }
  }
}
ctrlPlay();

//设置左右箭头手动切换图片
function clickPlay() {
  left.onclick = function () {
    clearInterval(timer); // 清除定时器
    //实现循环
    if (index <= 0) {
      index = span.length - 1;
    } else {
      index--;
    }
    show();
  }
//为右边箭头设置点击时间
  right.onclick = function () {
    clearInterval(timer); // 清除定时器
    //当index累加到圆点的数量时，将index置为0，从头开始，实现循环
    index++;
    if (index >= span.length) {
      index = 0;
    }
    show();
  }
}
clickPlay();
  //为每个小圆点都设置事件监听
function eventList() {
  for (let i = 0; i < span.length; i++) {
    //设置鼠标移入监听
    span[i].addEventListener('mouseenter', function () {
      //清除自动播放效果
      clearInterval(timer);
      index = i;
      //显示当前图片
      show();
    }, false);
    //设置鼠标移出监听
    span[i].addEventListener('mousemove', function () {
      //清除自动播放效果
      clearInterval(timer);
      //设置鼠标移出后从当前位置开始自动播放
      autoPlay();
    }, false);
  }
}
eventList();