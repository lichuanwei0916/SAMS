layui.use(['table', 'form'], function () {
    var table = layui.table;
    var form = layui.form;
    var athleteNo = null; // 运动员学号
    var status = null; // 检录状态

    // 显示加载符号
    var index = layer.load(0, {
        shade: [0.3, '#fff'],
    });
    form.render(); // 渲染全部表单

    $.ajax({
        url: '../../SearchAthleteCompetitionInformation',
        method: 'POST',
        dataType: 'json',
        data: {
            ci_id: document.getElementById("ci_id").value
        },
        success: function (res) {
            // 初始化运动会日程表
            table.render({
                elem: '#inputScoresTable',
                data: res.data,
                toolbar: '#toolbarDemo',//开启工具栏
                cellMinWidth: 85, // 设置单元格最小宽度为100px
                defaultToolbar: ['filter', 'exports', 'print', {
                    title: '提示',
                    layEvent: 'LAYTABLE_TIPS',
                    icon: 'layui-icon-tips'
                }],
                skin:'line',
                cols: [[
                    {field: 'num', width: 85, title: '序号',sort: true},
                    {field: 'a_name', width: 100, title: '姓名'},
                    {field: 'a_no', width: 120, title: '学号'},
                    {field: 'recordingStatus', width: 90, title: '检录状态'},
                    {field: 'recordingTime', width: 200, title: '检录时间'},
                    {field: 'group', width: 100, title: '组号', sort: true},
                    {field: 'runway', width: 100, title: '跑道'},
                    {field: 'grade', width: 150, title: '成绩'},
                    {field: 'operation', width: 200, title: '操作', toolbar: '#InputScoreOperationBar'},
                ]],
                page: true
            });
            layer.close(index);
        },
        error: function (err) {
            layer.msg( '查询数据时发生错误，请稍后再试！');
        }
    });
    layer.close(index);//关闭弹窗

    table.on('toolbar(inputScoresTable)', function (obj) {
        var id = obj.config.id;
        var checkStatus = table.checkStatus(id);
        var othis = lay(this);
        switch (obj.event) {
            case 'LAYTABLE_TIPS':
                layer.alert('请裁判长在项目开始后完成成绩的录入');
                break;
        }
        ;
    });

    table.on('tool(inputScoresTable)', function (obj) {
        var data = obj.data; // 获得当前行数据
        // 获取当前选中行的索引值

        // console.log(obj)
        if(obj.event === 'inputScore'){
            layer.prompt({title: '请输入成绩'}, function(value, index, elem){
                if(value === '') return elem.focus();
                $.ajax({
                    url: '../../UpdateAthleteCompetitionGrade',
                    data: {
                        ci_id: document.getElementById("ci_id").value,
                        id : data.id,
                        score : value,
                    },
                    method: 'POST',
                    dataType: 'json',
                    success: function (result) {
                        if (result.code === 200) { // 修改成功
                            layer.msg('录入成功');
                            layer.close(index);
                        } else{
                            layer.msg(result.msg);
                        }

                        $.ajax({
                            url: '../../SearchAthleteCompetitionInformation',
                            method: 'POST',
                            dataType: 'json',
                            data: {
                                ci_id: document.getElementById("ci_id").value
                            },
                            success: function (res) {
                                table.reload('inputScoresTable',{
                                    page: {
                                        curr: 1 // 重新从第 1 页开始
                                    },
                                    data: res.data
                                });
                            },
                            error: function (err) {
                                layer.msg( '查询数据时发生错误，请稍后再试！');
                            }
                        });
                    },
                    error: function (err) {
                        layer.msg( '录入数据时发生错误，请稍后再试！');
                    }
                });

            });
        }
    });

    // 监听搜索按钮的点击事件
    form.on('submit(searchGroup)', function (data) {
        athleteNo = data.field.athleteNo; // 运动员学号
        status = data.field.status; // 检录状态

        $.ajax({
            url: '../../SearchAthleteCompetitionInformation',
            data: {
                ci_id: document.getElementById("ci_id").value,
                athleteNo: athleteNo,
                status: status
            },
            method: 'POST',
            dataType: 'json',
            success: function (res) {
                table.reloadData('inputScoresTable',{
                    data: res.data
                });
            },
            error: function (err) {
                layer.msg( '查询数据时发生错误，请稍后再试！');
            }
        });

        // 渲染表格
        return false;
    });
})