layui.use(['table', 'form'], function () {
    var table = layui.table;
    var form = layui.form;

    // 显示加载符号
    var index = layer.load(0, {
        shade: [0.3, '#fff'],
    });

    $.ajax({
        url: '../../SearchAthleteGrade',
        method: 'POST',
        dataType: 'json',
        success: function (res) {
            // 初始化成绩表
            table.render({
                elem: '#ScoresTable',
                data: res.data,
                toolbar: '#toolbarDemo',//开启工具栏
                cellMinWidth: 85, // 设置单元格最小宽度为100px
                defaultToolbar: ['filter', 'exports', 'print', {
                    title: '提示',
                    layEvent: 'LAYTABLE_TIPS',
                    icon: 'layui-icon-tips'
                }],
                skin:'line',
                cols: [[
                    {field: 'num', width: 85, title: '序号',sort: true},
                    {field: 'a_name', width: 100, title: '姓名'},
                    {field: 'si_name', width: 120, title: '比赛名字'},
                    {field: 'ci_time', width: 200, title: '比赛时间'},
                    {field: 'recordingStatus', width: 90, title: '检录状态'},
                    {field: 'group', width: 100, title: '组号', sort: true},
                    {field: 'runway', width: 100, title: '跑道'},
                    {field: 'grade', width: 150, title: '成绩'}
                ]],
                page: true
            });
            layer.close(index);
        },
        error: function (err) {
            layer.msg( '查询数据时发生错误，请稍后再试！');
        }
    });
    layer.close(index);//关闭弹窗

    table.on('toolbar(ScoresTable)', function (obj) {
        var id = obj.config.id;
        var checkStatus = table.checkStatus(id);
        var othis = lay(this);
        switch (obj.event) {
            case 'LAYTABLE_TIPS':
                layer.alert('请在发布成绩后查询比赛成绩');
                break;
        }
        ;
    });

})