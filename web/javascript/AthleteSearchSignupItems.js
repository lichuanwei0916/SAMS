layui.use(['table', 'form'], function () {
    var table = layui.table;
    var form = layui.form;
    var projectID = null; // 项目名称
    var auditStatus = null; // 审核状态

    // 显示加载符号
    var index = layer.load(0, {
        shade: [0.3, '#fff'],
    });

    $.ajax({
        url: '../../SearchItemList',
        type: 'GET',
        dataType: 'json',
        success: function(data) {
            // 动态添加新的选项
            $.each(data, function(index, item) {
                var option = $('<option></option>').val(item.id).text(item.name);
                $('.project').append(option);
            });
            form.render();
        },
        error: function(xhr, textStatus, errorThrown) {
            alert('获取数据失败');
        }
    });
    $.ajax({
        url: '../../searchSignUpItem',
        data: {
            s_item: projectID,
            s_status: auditStatus
        },
        method: 'POST',
        dataType: 'json',
        success: function (res) {
            table.render({
                elem: '#dataTable',
                data: res.data,
                toolbar: '#toolbarDemo',//开启工具栏
                cellMinWidth: 100, // 设置单元格最小宽度为100px
                defaultToolbar: ['filter', 'exports', 'print', {
                    title: '提示',
                    layEvent: 'LAYTABLE_TIPS',
                    icon: 'layui-icon-tips'
                }],
                cols: [[
                    // {field: 'id', width: 100, title: '报名序号',hide:true},
                    {field: 'name', width: 100, title: '姓名',},
                    {field: 'no', width: 100, title: '学/工号'},
                    {field: 'event', width: 100, title: '报名项目'},
                    {field: 'status', width: 120, title: '审核状态', sort: true},
                    {field: 'signUpDate', width: 200, title: '报名时间', sort: true},
                    {field: 'auditDate', width: 200, title: '审核时间', sort: true},
                    {field: 'currentTableBar', width: 190, title: '操作',toolbar: '#currentTableBar'},
                ]],

                page: {
                    layout: ['limit', 'count', 'prev', 'page', 'next', 'skip'], //自定义分页布局
                    groups: 5, //只显示 1 个连续页码
                    first: false, //不显示首页
                    last: false, //不显示尾页
                    limit: 10, //每页显示10条记录
                    curr: 1 //设定初始在第 1 页（可选）
                },
                skin: 'line'
            });
            layer.close(index);
        },
        error: function (err) {
            layer.msg( '查询数据时发生错误，请稍后再试！');
        }
    });

    // 监听搜索按钮的点击事件
    form.on('submit(search)', function (data) {
        projectID = data.field.project; // 项目名称
        auditStatus = data.field.auditStatus; // 审核状态

        $.ajax({
            url: '../../searchSignUpItem',
            data: {
                s_item: projectID,
                s_status: auditStatus
            },
            method: 'POST',
            dataType: 'json',
            success: function (res) {
                table.reloadData('dataTable',{
                    data: res.data
                });
            },
            error: function (err) {
                layer.msg( '查询数据时发生错误，请稍后再试！');
            }
        });

        // 渲染表格
        return false;
    });

    table.on('tool(dataTable)', function (obj) {
        var data = obj.data; // 获得当前行数据
        // 获取当前选中行的索引值

        // console.log(obj)
        if(obj.event === 'edit'){
            if(data.status === "已通过" || data.status === "未通过"){
                layer.alert('项目已审核，如果要修改报名信息请联系运动员领队', {
                    time: 5*1000,
                    success: function(layero, index){
                        var timeNum = this.time/1000, setText = function(start){
                            layer.title('<span class="layui-font-red">'+ (start ? timeNum : --timeNum) + '</span> 秒后自动关闭', index);
                        };
                        setText(!0);
                        this.timer = setInterval(setText, 1000);
                        if(timeNum <= 0) clearInterval(this.timer);
                    },
                    end: function(){
                        clearInterval(this.timer);
                    }
                });
            }
            else {

            }
        } else if(obj.event === 'delete'){
            // 更多 - 下拉菜单
            if(data.status === "已通过" || data.status === "未通过"){
                layer.alert('项目已审核，如果要删除报名信息请联系运动员领队', {
                    time: 5*1000,
                    success: function(layero, index){
                        var timeNum = this.time/1000, setText = function(start){
                            layer.title('<span class="layui-font-red">'+ (start ? timeNum : --timeNum) + '</span> 秒后自动关闭', index);
                        };
                        setText(!0);
                        this.timer = setInterval(setText, 1000);
                        if(timeNum <= 0) clearInterval(this.timer);
                    },
                    end: function(){
                        clearInterval(this.timer);
                    }
                });
            }
            else{
                layer.confirm('确定删除此条报名记录？', {icon: 3}, function(){
                    $.ajax({
                        url: '../../deleteSignUpRecord',
                        type: 'GET',
                        data: {
                            sr_id: data.id,
                        },
                        success: function (result) {
                            if (result.code === 200) { // 删除成功
                                layer.msg('删除成功');

                                $.ajax({
                                    url: '../../searchSignUpItem',
                                    data: {
                                        s_item: projectID,
                                        s_status: auditStatus
                                    },
                                    method: 'POST',
                                    dataType: 'json',
                                    success: function (res) {
                                        table.reload('dataTable',{
                                            data: res.data
                                        });
                                    },
                                    error: function (err) {
                                        layer.msg( '查询数据时发生错误，请稍后再试！');
                                    }
                                });
                            } else {
                                layer.msg('删除失败');
                            }
                            // 成功获取到页面内容后执行的回调函数
                        },
                        error: function () {
                            layer.msg('删除失败,数据库存在问题');
                        }
                    });
                }, function(){
                    layer.msg('取消删除');
                });

            }
            // layer.msg("data");
        }
    });

    table.on('toolbar(dataTable)', function (obj) {
        var id = obj.config.id;
        var checkStatus = table.checkStatus(id);
        var othis = lay(this);
        switch (obj.event) {
            case 'LAYTABLE_TIPS':
                layer.alert('输入相关信息以查询运动会报名信息');
                break;
        }
        ;
    });
});
