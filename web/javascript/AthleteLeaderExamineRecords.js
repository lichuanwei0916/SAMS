layui.use(['table', 'form'], function () {
    var table = layui.table;
    var form = layui.form;
    var athleteName = null; // 名字
    var projectName = null; // 项目名称
    var auditStatus = null; // 审核状态

    // 显示加载符号
    var index = layer.load(0, {
        shade: [0.3, '#fff'],
    });

    // 在Ajax请求成功后，将查询结果添加到下拉列表中的选项中
    $.ajax({
        url: '../../SearchItemList',
        type: 'GET',
        dataType: 'json',
        success: function(data) {
            // 动态添加新的选项
            $.each(data, function(index, item) {
                var option = $('<option></option>').val(item.id).text(item.name);
                $('.project').append(option);
            });
            form.render();
        },
        error: function(xhr, textStatus, errorThrown) {
            alert('获取数据失败');
        }
    });
    $.ajax({
        url: '../../searchSignUpItem',
        data: {
            s_item: projectName,
            s_status: auditStatus
        },
        method: 'POST',
        dataType: 'json',
        success: function (res) {
            table.render({
                elem: '#dataTable',
                data: res.data,
                toolbar: '#toolbarDemo',//开启工具栏
                cellMinWidth: 90, // 设置单元格最小宽度为100px
                defaultToolbar: ['filter', 'exports', 'print', {
                    title: '提示',
                    layEvent: 'LAYTABLE_TIPS',
                    icon: 'layui-icon-tips'
                }],
                cols: [[
                    {field: 'num', width: 90, title: '报名序号'},
                    {field: 'name', width: 100, title: '姓名',sort: true},
                    {field: 'no', width: 110, title: '学/工号',sort: true},
                    {field: 'event', width: 120, title: '报名项目',sort: true},
                    {field: 'status', width: 120, title: '审核状态', sort: true},
                    {field: 'signUpDate', width: 170, title: '报名时间', sort: true},
                    {field: 'bestGrade', width: 170, title: '最佳成绩', sort: true},
                    {field: 'operationBar', width: 190, title: '操作',toolbar: '#operationBar'},
                ]],

                page: {
                    layout: ['limit', 'count', 'prev', 'page', 'next', 'skip'], //自定义分页布局
                    groups: 5, //只显示 1 个连续页码
                    first: false, //不显示首页
                    last: false, //不显示尾页
                    limit: 10, //每页显示10条记录
                    curr: 1 //设定初始在第 1 页（可选）
                },
                skin: 'line'
            });
            layer.close(index);
        },
        error: function (err) {
            layer.msg( '查询数据时发生错误，请稍后再试！');
        }
    });

    // 监听搜索按钮的点击事件
    form.on('submit(search)', function (data) {
        athleteName = data.field.athleteName;
        projectName = data.field.projectName; // 项目名称
        auditStatus = data.field.auditStatus; // 审核状态

        $.ajax({
            url: '../../searchSignUpItem',
            data: {
                a_athleteName:athleteName,
                s_item: projectName,
                s_status: auditStatus
            },
            method: 'POST',
            dataType: 'json',
            success: function (res) {
                table.reloadData('dataTable',{
                    data: res.data
                });
            },
            error: function (err) {
                layer.msg( '查询数据时发生错误，请稍后再试！');
            }
        });

        // 渲染表格
        return false;
    });

    table.on('toolbar(dataTable)', function (obj) {
        var id = obj.config.id;
        var checkStatus = table.checkStatus(id);
        var othis = lay(this);
        switch (obj.event) {
            case 'LAYTABLE_TIPS':
                layer.alert('请运动员领队根据自己的学院的实际情况审核报名');
                break;
        }
        ;
    });

    table.on('tool(dataTable)', function (obj) {
        var data = obj.data; // 获得当前行数据
        // 获取当前选中行的索引值

        // console.log(obj)
        if(obj.event === 'audit'){
            layer.confirm('确定通过此条报名记录？', {icon: 3}, function(){
                $.ajax({
                    url: '../../approveSignUpRecord',
                    type: 'POST',
                    data: {
                        sr_id: data.id,
                    },
                    success: function (result) {
                        if (result.code === 200) { // 删除成功
                            layer.msg('审核成功');

                            $.ajax({
                                url: '../../searchSignUpItem',
                                data: {
                                    s_item: projectName,
                                    s_status: auditStatus
                                },
                                method: 'POST',
                                dataType: 'json',
                                success: function (res) {
                                    table.reload('dataTable',{
                                        data: res.data
                                    });
                                },
                                error: function (err) {
                                    layer.msg( '查询数据时发生错误，请稍后再试！');
                                }
                            });
                        }
                        else if (result.code === 100){
                            layer.msg(result.msg);
                        }
                    },
                    error: function (result) {
                        layer.msg('通过失败,数据库存在问题');
                    }
                });
            }, function(){
                layer.msg('取消通过');
            });
        }
        else if(obj.event === 'reject'){
            layer.confirm('确定拒绝通过此条报名记录？', {icon: 3}, function(){
                $.ajax({
                    url: '../../notApproveSignUpRecord',
                    type: 'POST',
                    data: {
                        sr_id: data.id,
                    },
                    success: function (result) {
                        if (result.code === 200) { // 删除成功
                            layer.msg('审核成功:不通过');

                            $.ajax({
                                url: '../../searchSignUpItem',
                                data: {
                                    s_item: projectName,
                                    s_status: auditStatus
                                },
                                method: 'POST',
                                dataType: 'json',
                                success: function (res) {
                                    table.reload('dataTable',{
                                        data: res.data
                                    });
                                },
                                error: function (err) {
                                    layer.msg( '查询数据时发生错误，请稍后再试！');
                                }
                            });
                        }
                        else if (result.code === 100){
                            layer.msg(result.msg);
                        }
                    },
                    error: function (result) {
                        layer.msg('通过失败,数据库存在问题');
                    }
                });
            }, function(){

            });
        }
        else if(obj.event === 'quash'){
            layer.confirm('确定撤销审核这条报名记录？', {icon: 3}, function(){
                $.ajax({
                    url: '../../quashApproveSignUpRecord',
                    type: 'POST',
                    data: {
                        sr_id: data.id,
                    },
                    success: function (result) {
                        if (result.code === 200) { // 删除成功
                            layer.msg('撤销成功');

                            $.ajax({
                                url: '../../searchSignUpItem',
                                data: {
                                    s_item: projectName,
                                    s_status: auditStatus
                                },
                                method: 'POST',
                                dataType: 'json',
                                success: function (res) {
                                    table.reload('dataTable',{
                                        data: res.data
                                    });
                                },
                                error: function (err) {
                                    layer.msg( '查询数据时发生错误，请稍后再试！');
                                }
                            });
                        }
                        else if (result.code === 100){
                            layer.msg(result.msg);
                        }
                    },
                    error: function (result) {
                        layer.msg('撤销失败,数据库存在问题');
                    }
                });
            }, function(){

            });
        }
    });
})