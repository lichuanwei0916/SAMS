layui.use(['table', 'form'], function () {
    var table = layui.table;
    var form = layui.form;

    // 显示加载符号
    var index = layer.load(0, {
        shade: [0.3, '#fff'],
    });

    // 在Ajax请求成功后，刷新表格
    $.ajax({
        url: '../../SearchCompetitionInformation',
        method: 'POST',
        dataType: 'json',
        success: function (res) {
            // 初始化运动会日程表
            table.render({
                elem: '#schedule-table',
                data: res.data,
                toolbar: '#toolbarDemo',//开启工具栏
                cellMinWidth: 100, // 设置单元格最小宽度为100px
                defaultToolbar: ['filter', 'exports', 'print', {
                    title: '提示',
                    layEvent: 'LAYTABLE_TIPS',
                    icon: 'layui-icon-tips'
                }],
                skin:'line',
                cols: [[
                    {field: 'num', width: 130, title: '比赛序号',sort: true},
                    {field: 'itemName', width: 130, title: '比赛名称', sort: true},
                    {field: 'nature', width: 130, title: '比赛性质'},
                    {field: 'groupName', width: 150, title: '比赛组别', sort: true},
                    {field: 'venue', width: 110, title: '比赛地点'},
                    {field: 'time', width: 230, title: '比赛时间', sort: true},
                ]],
                page: true
            });
            layer.close(index);
        },
        error: function (err) {
            layer.msg( '查询数据时发生错误，请稍后再试！');
        }
    });

    table.on('toolbar(schedule-table)', function (obj) {
        var id = obj.config.id;
        var checkStatus = table.checkStatus(id);
        var othis = lay(this);
        switch (obj.event) {
            case 'LAYTABLE_TIPS':
                layer.alert('查看运动会日程详细安排');
                break;
        }
        ;
    });

})