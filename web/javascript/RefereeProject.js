layui.use(['table', 'form'], function () {
    var table = layui.table;
    var form = layui.form;
    var si_id = null; // 项目id
    var g_id = null; // 组别id

    // 显示加载符号
    var index = layer.load(0, {
        shade: [0.3, '#fff'],
    });

    // 在Ajax请求成功后，将查询结果添加到下拉列表中的选项中
    $.ajax({
        url: '../../SearchItemList',
        type: 'GET',
        dataType: 'json',
        success: function(data) {
            // 动态添加新的选项
            $.each(data, function(index, item) {
                var option = $('<option></option>').val(item.id).text(item.name);
                $('.project').append(option);
            });
            form.render();
        },
        error: function(xhr, textStatus, errorThrown) {
            alert('获取数据失败');
        }
    });
    $.ajax({
        url: '../../SearchGroupList',
        type: 'GET',
        dataType: 'json',
        success: function(data) {
            // 动态添加新的选项
            $.each(data, function(index, item) {
                var option = $('<option></option>').val(item.id).text(item.name);
                $('.group').append(option);
            });
            // 相关操作
            form.render(); // 渲染全部表单
        },
        error: function(xhr, textStatus, errorThrown) {
            alert('获取数据失败');
        }
    });
    $.ajax({
        url: '../../SearchRefereeCompetitionInformation',
        method: 'POST',
        dataType: 'json',
        success: function (res) {
            table.render({
                elem: '#dataTable',
                data: res.data,
                toolbar: '#toolbarDemo',//开启工具栏
                cellMinWidth: 90, // 设置单元格最小宽度为100px
                defaultToolbar: ['filter', 'exports', 'print', {
                    title: '提示',
                    layEvent: 'LAYTABLE_TIPS',
                    icon: 'layui-icon-tips'
                }],
                cols: [[
                    {field: 'num', width: 90, title: '序号',sort: true},
                    {field: 'si_name', width: 120, title: '项目名字',sort: true},
                    {field: 'venue', width: 120, title: '地点'},
                    {field: 'groupName', width: 120, title: '项目组别', sort: true},
                    {field: 'nature', width: 115, title: '项目性质', sort: true},
                    {field: 'startTime', width: 180, title: '开始时间',sort: true},
                    {field: 'issue', width: 120, title: '发布成绩', sort: true},
                    {field: 'operationBar', width: 160, title: '操作',toolbar: '#operationBar'},
                ]],

                page: {
                    layout: ['limit', 'count', 'prev', 'page', 'next', 'skip'], //自定义分页布局
                    groups: 5, //只显示 1 个连续页码
                    first: false, //不显示首页
                    last: false, //不显示尾页
                    limit: 10, //每页显示10条记录
                    curr: 1 //设定初始在第 1 页（可选）
                },
                skin: 'line'
            });
            layer.close(index);
        },
        error: function (err) {
            layer.msg( '查询数据时发生错误，请稍后再试！');
        }
    });

    // 监听搜索按钮的点击事件
    form.on('submit(search)', function (data) {
        si_id = data.field.project;
        g_id = data.field.group;// 组别id

        $.ajax({
            url: '../../SearchRefereeCompetitionInformation',
            data: {
                si_id: si_id,
                g_id: g_id
            },
            method: 'POST',
            dataType: 'json',
            success: function (res) {
                table.reloadData('dataTable',{
                    data: res.data
                });
            },
            error: function (err) {
                layer.msg( '查询数据时发生错误，请稍后再试！');
            }
        });

        // 渲染表格
        return false;
    });

    table.on('toolbar(dataTable)', function (obj) {
        var id = obj.config.id;
        var checkStatus = table.checkStatus(id);
        var othis = lay(this);
        switch (obj.event) {
            case 'LAYTABLE_TIPS':
                layer.alert('请裁判长在项目开始后完成成绩的录入');
                break;
        }
        ;
    });

    table.on('tool(dataTable)', function (obj) {
        var data = obj.data; // 获得当前行数据
        // 获取当前选中行的索引值

        // console.log(obj)
        if(obj.event === 'input'){
            $.ajax({
                url: '../../jsp/Referee/InputAthleteScores.jsp',
                type: 'GET',
                data: {
                    ci_id:data.ci_id
                },
                dataType: 'html',
                success: function (content) {
                    // 成功获取到页面内容后执行的回调函数
                    layer.open({
                        title: '录入成绩',
                        type: 1,
                        maxmin: true,
                        area: ['90%', '90%'],
                        content: content,
                        offset: ['50px', '100px'],
                        scrollbar:false
                    });
                },
                error: function () {
                    layer.msg('Failed to load the page');
                }
            });
        }
    });
})