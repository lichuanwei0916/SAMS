// 通过监听输入框上的 input 事件来判断输入框中是否有内容，如果有则将其文字颜色调整为黑色
$(document).ready(function() {
    // $(document).ready() 方法会在页面文档加载完成后自动执行传入的回调函数，因此可以确保代码的所有依赖项都已经加载完成并可用。
    $('.input').on('input', function() {
        // $() 是 jQuery 的选择器语法，它可以根据给定的 CSS 选择器获取匹配的元素。在本例中，$('#input_id') 获取 ID 为 "input_id" 的输入框元素。
        // .on('input', function() { ... }) 方法会将一个输入框的 input 事件与一个回调函数进行绑定。在本例中，每当输入框元素触发 input 事件时（即每次输入文字、删除文字等），传入的函数就会被执行。
        if ($(this).val() !== '') {
            $(this).css('color', 'black');
            // 修改字体颜色的 jQuery 方法
        } else {
            $(this).css('color', ''); // 如果想恢复字体默认颜色，可以将值设置为空字符串或者不设置
            // 表示移除 font-color CSS 属性，以便回到输入框的默认字体颜色
        }
    });
});