layui.use(['table', 'form'], function () {
    var table = layui.table;
    var form = layui.form;

    // 显示加载符号
    var index = layer.load(0, {
        shade: [0.3, '#fff'],
    });

    // 在Ajax请求成功后，刷新表格
    $.ajax({
        url: '../../SearchCompetitionInformation',
        method: 'POST',
        dataType: 'json',
        success: function (res) {
            // 初始化运动会日程表
            table.render({
                elem: '#schedule-table',
                data: res.data,
                toolbar: '#toolbarDemo',//开启工具栏
                cellMinWidth: 100, // 设置单元格最小宽度为100px
                defaultToolbar: ['filter', 'exports', 'print', {
                    title: '提示',
                    layEvent: 'LAYTABLE_TIPS',
                    icon: 'layui-icon-tips'
                }],
                skin:'line',
                cols: [[
                    {field: 'num', width: 120, title: '比赛序号',sort: true},
                    {field: 'itemName', width: 120, title: '比赛名称', sort: true},
                    {field: 'nature', width: 120, title: '比赛性质'},
                    {field: 'groupName', width: 140, title: '比赛组别', sort: true},
                    {field: 'venue', width: 100, title: '比赛地点'},
                    {field: 'time', width: 220, title: '比赛时间', sort: true},
                    {field: 'operation', width: 200, title: '操作', toolbar: '#operationBar'},
                ]],
                page: true
            });
            layer.close(index);
        },
        error: function (err) {
            layer.msg( '查询数据时发生错误，请稍后再试！');
        }
    });

    table.on('tool(schedule-table)', function (obj) {
        var data = obj.data; // 获得当前行数据
        var category,nature;

        if(data.category === '个人赛')
            category = 0;
        else if(data.category === '团体赛')
            category = 1;

        if(data.nature === '预赛')
            nature = 1;
        else if(data.nature === '决赛')
            nature = 2;
        else if(data.nature === '预决赛')
            nature = 3;

        // console.log(obj)
        if (obj.event === 'edit') {
            $.ajax({
                url: '../../jsp/HeadReferee/updateSchedule.jsp',
                type: 'GET',
                data: {
                    id: data.id,
                    name: data.itemName,
                    projectId:data.itemId,
                    time:data.time,
                    venue:data.venue,
                    itemProperty: category,
                    nature: nature,
                    peopleLimit:data.limit,
                    groupID:data.group,
                },
                dataType: 'html',
                success: function (content) {
                    // 成功获取到页面内容后执行的回调函数
                    layer.open({
                        title: '编辑运动会日程',
                        type: 1,
                        maxmin: true,
                        area: ['70%', '80%'],
                        content: content,
                        offset: ['50px', '330px'],
                        scrollbar:false
                    });
                },
                error: function () {
                    layer.msg('Failed to load the page');
                }
            });
        }
        else if (obj.event === 'delete') {
            $.ajax({
                url: '',
                type: 'GET',
                data: {
                    id: data.id,
                },
                success: function (result) {
                    // 成功获取到页面内容后执行的回调函数
                    if (result.code === 200) { // 成功
                        layer.msg('删除成功');
                        $.ajax({
                            url: '',
                            method: 'POST',
                            dataType: 'json',
                            success: function (res) {
                                table.reload('dataTable',{
                                    page: {
                                        curr: 1 // 重新从第 1 页开始
                                    },
                                    data: res.data
                                });
                            },
                            error: function (err) {
                                layer.msg( '查询数据时发生错误，请稍后再试！');
                            }
                        });
                    } else if(result.code === 100){
                        layer.confirm('该项目有对应的报名记录，确认要删除项目？', {icon: 3}, function(){
                            $.ajax({
                                url: '',
                                type: 'GET',
                                data: {
                                    id: data.id,
                                },
                                success: function (result) {
                                    // 成功获取到页面内容后执行的回调函数
                                    if (result.code === 200) { // 成功
                                        layer.msg('删除成功');
                                        $.ajax({
                                            url: '',
                                            method: 'POST',
                                            dataType: 'json',
                                            success: function (res) {
                                                table.reload('dataTable',{
                                                    page: {
                                                        curr: 1 // 重新从第 1 页开始
                                                    },
                                                    data: res.data
                                                });
                                            },
                                            error: function (err) {
                                                layer.msg( '查询数据时发生错误，请稍后再试！');
                                            }
                                        });
                                    }
                                    else
                                        layer.msg('删除失败');
                                },
                                error: function () {
                                    layer.msg('删除失败');
                                }
                            });
                        }, function(){
                            layer.msg('取消删除');
                        });
                    }
                },
                error: function () {
                    layer.msg('删除失败');
                }
            });

        }
    });
    table.on('toolbar(schedule-table)', function (obj) {
        var id = obj.config.id;
        var checkStatus = table.checkStatus(id);
        var othis = lay(this);
        switch (obj.event) {
            case 'LAYTABLE_TIPS':
                layer.alert('查看运动会日程详细安排');
                break;
        }
        ;
    });


    $('.addSchedule').click(function(){
        $.ajax({
            url: '../../jsp/HeadReferee/addSchedule.jsp',
            type: 'GET',
            dataType: 'html',
            success: function (content) {
                // 成功获取到页面内容后执行的回调函数
                layer.open({
                    title: '添加运动会日程',
                    type: 1,
                    maxmin: true,
                    area: ['70%', '80%'],
                    content: content,
                    offset: ['50px', '330px'],
                    scrollbar:false
                });
            },
            error: function () {
                layer.msg('Failed to load the page');
            }
        });
    });
})