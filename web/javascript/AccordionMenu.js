var list = document.querySelector('.menu_list');
var menus = list.querySelectorAll('.menu_head');
for (let i = 0; i < menus.length; i++) {
    menus[i].addEventListener('click', function() {
        let alinks = this.nextElementSibling.children;
        if (this.className === 'menu_head') {
            this.className = 'menu_head current';
            this.classList.add("active"); // 添加 active 类
            for (let k = 0; k < alinks.length; k++) {
                alinks[k].style.height = '38px';
                alinks[k].style.borderBottom='1px solid #e0e0e0';
            }
        }
        else {
            this.className = 'menu_head';
            for (let k = 0; k < alinks.length; k++) {
                alinks[k].style.height = '0';
                alinks[k].style.borderBottom = '0';
            }
        }
        for (let j = 0; j < menus.length; j++) { //排他思想
            if (j != i) {
                menus[j].className = 'menu_head';
                alist = menus[j].nextElementSibling.children;
                for (let k = 0; k < alist.length; k++) {
                    alist[k].style.height = '0';
                    alist[k].style.borderBottom = '0';
                }
            }
        }
    })
}