layui.use(['table', 'form'], function () {
    var table = layui.table;
    var form = layui.form;
    var athleteNo = null; // 运动员学号
    var status = null; // 检录状态

    // 显示加载符号
    var index = layer.load(0, {
        shade: [0.3, '#fff'],
    });

    form.render(); // 渲染全部表单

    $.ajax({
        url: '../../SearchRefereeCompetitionInformation',
        method: 'POST',
        dataType: 'json',
        data: {
            ci_id: document.getElementById("ci_id").value
        },
        success: function (res) {
            table.render({
                elem: '#editRefereesTable',
                data: res.data,
                toolbar: '#toolbarDemo',//开启工具栏
                cellMinWidth: 90, // 设置单元格最小宽度为100px
                defaultToolbar: ['filter', 'exports', 'print', {
                    title: '提示',
                    layEvent: 'LAYTABLE_TIPS',
                    icon: 'layui-icon-tips'
                }],
                cols: [[
                    {field: 'num', width: 90, title: '序号',sort: true},
                    {field: 'si_name', width: 120, title: '项目名字'},
                    {field: 'nature', width: 115, title: '项目性质'},
                    {field: 'groupName', width: 115, title: '项目组别'},
                    {field: 'r_id', width: 115, title: '裁判ID',sort: true},
                    {field: 'r_name', width: 115, title: '裁判名字',sort: true},
                    {field: 'startTime', width: 180, title: '开始时间',sort: true},
                    {field: 'operationBar', width: 160, title: '操作',toolbar: '#editRefereesOperationBar'},
                ]],

                page: {
                    layout: ['limit', 'count', 'prev', 'page', 'next', 'skip'], //自定义分页布局
                    groups: 5, //只显示 1 个连续页码
                    first: false, //不显示首页
                    last: false, //不显示尾页
                    limit: 10, //每页显示10条记录
                    curr: 1 //设定初始在第 1 页（可选）
                },
                skin: 'line'
            });
            layer.close(index);
        },
        error: function (err) {
            layer.msg( '查询数据时发生错误，请稍后再试！');
        }
    });
    layer.close(index);//关闭弹窗

    table.on('toolbar(editRefereesTable)', function (obj) {
        var id = obj.config.id;
        var checkStatus = table.checkStatus(id);
        var othis = lay(this);
        switch (obj.event) {
            case 'LAYTABLE_TIPS':
                layer.alert('请编辑项目裁判信息');
                break;
        }
        ;
    });

    table.on('tool(editRefereesTable)', function (obj) {
        var data = obj.data; // 获得当前行数据
        // 获取当前选中行的索引值

        // console.log(obj)
        if(obj.event === 'inputScore'){

        }
    });

    $('#addReferee').click(function(){
        var jsContent = "<script src=\"../../javascript/searchRefereeTable.js\"></script>";
        layer.open({
            type: 1,
            // area: ['420px', '240px'], // 宽高
            content: jsContent + '<div><input input type="text" name="searchReferees"  placeholder="请选择裁判员" id = "searchReferees"></input></div>'
        });
    });
})