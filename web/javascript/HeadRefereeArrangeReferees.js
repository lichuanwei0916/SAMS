layui.use(['table', 'form'], function () {
    var table = layui.table;
    var form = layui.form;

    // 显示加载符号
    var index = layer.load(0, {
        shade: [0.3, '#fff'],
    });

    // 在Ajax请求成功后，刷新表格
    $.ajax({
        url: '../../SearchCompetitionInformation',
        method: 'POST',
        dataType: 'json',
        success: function (res) {
            // 初始化运动会项目表
            table.render({
                elem: '#competition-table',
                data: res.data,
                toolbar: '#toolbarDemo',//开启工具栏
                cellMinWidth: 100, // 设置单元格最小宽度为100px
                defaultToolbar: ['filter', 'exports', 'print', {
                    title: '提示',
                    layEvent: 'LAYTABLE_TIPS',
                    icon: 'layui-icon-tips'
                }],
                skin:'line',
                cols: [[
                    {field: 'num', width: 120, title: '比赛序号',sort: true},
                    {field: 'itemName', width: 120, title: '比赛名称', sort: true},
                    {field: 'nature', width: 120, title: '比赛性质'},
                    {field: 'groupName', width: 140, title: '比赛组别', sort: true},
                    {field: 'venue', width: 100, title: '比赛地点'},
                    {field: 'time', width: 220, title: '比赛时间', sort: true},
                    {field: 'operation', width: 200, title: '操作', toolbar: '#operationBar'},
                ]],
                page: true
            });
            layer.close(index);
        },
        error: function (err) {
            layer.msg( '查询数据时发生错误，请稍后再试！');
        }
    });

    table.on('tool(competition-table)', function (obj) {
        var data = obj.data; // 获得当前行数据
        var category,nature;

        // console.log(obj)
        if (obj.event === 'editInspector') {
            layer.msg('项目开发中');
        }
        else if (obj.event === 'editReferee') {
            $.ajax({
                url: '../../jsp/HeadReferee/editReferees.jsp',
                type: 'GET',
                data: {
                    ci_id:data.id
                },
                dataType: 'html',
                success: function (content) {
                    // 成功获取到页面内容后执行的回调函数
                    layer.open({
                        title: '编辑运动会裁判员',
                        type: 1,
                        area: ['70%', '80%'],
                        content: content,
                        offset: ['50px', '330px'],
                        scrollbar:false
                    });
                },
                error: function () {
                    layer.msg('Failed to load the page');
                }
            });
        }
    });
    table.on('toolbar(competition-table)', function (obj) {
        var id = obj.config.id;
        var checkStatus = table.checkStatus(id);
        var othis = lay(this);
        switch (obj.event) {
            case 'LAYTABLE_TIPS':
                layer.alert('点击按钮安排项目检录员和裁判员');
                break;
        }
        ;
    });
})