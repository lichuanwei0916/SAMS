layui.use(['form', 'step', 'laydate'], function () {
    var $ = layui.$,
        form = layui.form,
        step = layui.step,
        laydate = layui.laydate;
    var carousel = layui.carousel;
    // 渲染 - 常规轮播
    carousel.render();

    /**
     * 初始化表单，要加上，不然刷新部分组件可能会不加载
     */
    // 在Ajax请求成功后，将查询结果添加到下拉列表中的选项中
    $.ajax({
        url: '../../SearchItemList',
        type: 'GET',
        dataType: 'json',
        success: function(data) {
            // 动态添加新的选项
            $.each(data, function(index, item) {
                var option = $('<option></option>').val(item.id).text(item.name);
                $('.project').append(option);
            });
            form.render();
        },
        error: function(xhr, textStatus, errorThrown) {
            alert('获取数据失败');
        }
    });

    //日期
    laydate.render({
        elem: '#birthday',
        theme: 'molv',
        format: 'yyyy-MM-dd',
        max: 0,
        min: '1900-1-1'
    });


    step.render({
        elem: '#stepForm',
        filter: 'stepForm',
        width: '100%', //设置容器宽度
        stepWidth: '750px',
        height: '500px',
        stepItems: [{
            title: '核对基本信息'
        }, {
            title: '填写报名信息'
        }, {
            title: '提交'
        }]
    });


    form.on('submit(formStep)', function (data) {
        step.next('#stepForm');
        return false;
    });

    form.on('submit(formStep2)', function (data) {
        step.next('#stepForm');
        return false;
    });

    $('.pre').click(function () {
        step.pre('#stepForm');
    });

    $('.next').click(function () {
        step.next('#stepForm');
    });

    $('#submit-btn').click(function (event) {
        // 防止表单默认提交
        event.preventDefault();

        // 获取表单数据
        var birthday = $('input[name="birthday"]').val();
        var phone = $('input[name="phone"]').val();
        var project = $('select[name="project"]').val();
        var bestgrade = $('input[name="bestgrade"]').val();

        // 发送 Ajax 请求
        $.ajax({
            type: 'POST',
            url: '../../addSignUpItem', // Servlet 的 URL
            data: {
                birthday: birthday,
                phone: phone,
                project: project,
                bestgrade: bestgrade
            },
            dataType: 'json',
            success: function (result) {
                // 处理返回结果
                if (result && result.code === 200) {
                    // 成功
                    layer.msg(result.msg || '报名成功！');
                    step.next('#stepForm');
                    // 清空文本框数据
                    $('select[name="project"]').val('');
                    $('input[name="bestgrade"]').val('');
                    form.render();
                    return true;
                } else if(result && result.code === -1){
                    // 失败
                    layer.msg(result.msg || '报名失败，表单未填写完整！');
                }
                else if(result && result.code === 1){
                    // 失败
                    layer.msg(result.msg || '报名失败，但个人数据已更新!');
                }
                else if(result && result.code === 2){
                    // 失败
                    layer.msg(result.msg || '报名成功，但个人数据未更新！');
                }
                else if(result && result.code === 3){
                    // 失败
                    layer.msg(result.msg || '报名失败，且个人数据未更新！');
                }
            },
            error: function (result) {
                // 处理错误信息
                layer.msg(result.msg || '提交表单数据时发生错误');
            }
        });
        return false;
    });
})

