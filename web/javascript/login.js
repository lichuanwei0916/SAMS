function getQueryVariable(variable)
{
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i=0;i<vars.length;i++) {
        var pair = vars[i].split("=");
        if(pair[0] == variable){
            return pair[1];
        }
    }
    return null;
}
$(window).on("load",function() {
    var isSuc = getQueryVariable("isSuc");
    if (isSuc!=null&&isSuc==0) {
        $('#btn').before($('<div style="color:red;font-size:10px;margin-top:10px;">登录失败！请检查角色、用户名或密码是否错误</div>'));
    }

});

var working = false;
var tempa = $('#inacc');
var tempi = $('#i1');
$('#btn').on('click', function (e) {
    setTimeout("$('.login').submit()","1500");
    e.preventDefault();
    if (working) return;
    working = true;
    var $this = $('.login'),
        $state = $this.find('button > .state');
    $this.addClass('loading');
    $state.html('Loading');
    setTimeout(function () {
        $this.addClass('ok');
        $state.html('Welcome back!');
        setTimeout(function () {
            $state.html('Log in');
            $this.removeClass('ok loading');
            working = false;
        }, 1000);
    }, 1000);
});