<%--
  Created by IntelliJ IDEA.
  User: Li Chuanwei
  Date: 2023-05-04
  Time: 23:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>运动会管理系统</title>
    <!-- 导入图标的css -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/lib/fontawesome/css/all.css">

    <%-- 导入jquery--%>
    <script src="https://cdn.staticfile.org/jquery/1.10.2/jquery.min.js"></script>
    <!-- 导入layui的css -->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/lib/layui/css/layui.css">
    <!-- 导入layui的js -->
    <script src="${pageContext.request.contextPath}/lib/layui/layui.js"></script>

    <link rel="stylesheet" type="text/css" href="../../css/AthleteSetPassword.css">
</head>
<body>
<%-- 检查是否已经登录，如果未登录则重定向到登录页面 --%>
<%
    session = request.getSession(false);
    if (session == null || session.getAttribute("user") == null) {
        response.sendRedirect("../unauthorized.jsp");
    }
%>
<%--导航栏--%>
<%@ include file="../headbar.jsp" %>

<div class="main">
    <div class="left">
        <jsp:include page="AthleteIndexMenu.jsp"/>
    </div>
    <div class="right">
        <div class="now-place">
            <i class="fas fa-map-marker-alt" style="font-size:1.2em;color:rgb(101,177,255);margin-right: 1vw"></i>
            <div>当前位置：修改密码</div>
        </div>
        <div class="blank"></div>
        <div class="layui-container">
            <jsp:include page="../SetPassword.jsp"/>
        </div>
    </div>
</div>
</body>
</html>
