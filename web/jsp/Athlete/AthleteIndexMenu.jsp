<%--
  Created by IntelliJ IDEA.
  User: Li Chuanwei
  Date: 2023-04-24
  Time: 22:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="modle.Athlete" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
  Athlete athlete = new Athlete();
  session = request.getSession(false);
  if (session == null || session.getAttribute("user") == null) {
    response.sendRedirect("../unauthorized.jsp");
  }
  else{
    athlete = (Athlete) session.getAttribute("role");
    if(athlete == null){
      athlete = new Athlete();
      response.sendRedirect("../unerror.jsp");
    }
  }
%>
<link rel="stylesheet" type="text/css" href="../../css/AthleteIndexMenu.css">
<script src="../../javascript/AccordionMenu.js" defer="true"></script>
<script src="../../javascript/AthleteIndexMenu.js" defer="true"></script>
<div class="left_user">
  <img class="left_user-avatar" src="../../img/boy.png" alt="运动员">
  <span class="left_user-name"><%=athlete.getName()%>(运动员)</span>
</div>
<ul class="menu_list">
  <li>
    <%--<dl>标签来创建一个术语列表--%>
    <dl class="menu_title">
      <a href="AthleteIndex.jsp">
        <i class="fas fa-home" style="font-size:1.2em;color:white;padding-right:0.7vw"></i>
        我的首页
      </a>
    </dl>
  </li>
  <li>
    <dl class="menu_head" id = "my_items">
      <i class="fas fa-running" style="font-size:1.3em;color:white;padding-right:1.2vw"></i>
      我的项目
    </dl>
    <dt class="menu_body">
      <a href="./signup_items.jsp" id = "signup_items">报名项目</a>
      <a href="./search_signup_items.jsp" id = "search_signup_items">查询报名</a>
      <a href="./searchSchedule.jsp" id = "searchSchedule">查询日程</a>
      <a href="./rules.jsp" id = "rules">项目规则</a>
    </dt>
  </li>
  <li>
    <dl class="menu_head" id = "my_grades">
      <i class="fas fa-trophy" style="font-size:1.2em;color:white;padding-right:0.7vw"></i>
      我的成绩
    </dl>
    <dt class="menu_body">
      <a href="./query_grades.jsp" id = "query_grades">成绩查询</a>
      <a href="./appeal_grades.jsp" id = "appeal_grades">成绩申诉</a>
    </dt>
  </li>
</ul>