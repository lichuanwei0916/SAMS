<%--
  Created by IntelliJ IDEA.
  User: Li Chuanwei
  Date: 2023-04-24
  Time: 23:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="modle.Athlete" %>
<html>
<head>
    <title>运动会管理系统</title>
    <!-- 导入图标的css -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/lib/fontawesome/css/all.css">

    <%-- 导入jquery--%>
    <script src="https://cdn.staticfile.org/jquery/1.10.2/jquery.min.js"></script>
    <!-- 导入layui的css -->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/lib/layui/css/layui.css">
    <!-- 导入layui的js -->
    <script src="${pageContext.request.contextPath}/lib/layui/layui.js"></script>

    <link rel="stylesheet" type="text/css" href="../../css/AthleteSignupItems.css">
    <script src="../../javascript/AthleteSignupItems.js"></script>
</head>
<body>
<%-- 检查是否已经登录，如果未登录则重定向到登录页面 --%>
<%
    Athlete athlete = new Athlete();
    session = request.getSession(false);
    if (session == null || session.getAttribute("user") == null) {
        response.sendRedirect("../unauthorized.jsp");
    }
    else{
        athlete = (Athlete) session.getAttribute("role");
        if(athlete == null){
            athlete = new Athlete();
            response.sendRedirect("../unerror.jsp");
        }
    }
%>
<%--导航栏--%>
<%@ include file="../headbar.jsp" %>

<div class="main">
    <div class="left">
        <jsp:include page="AthleteIndexMenu.jsp"/>
    </div>
    <div class="right">
        <div class="now-place">
            <i class="fas fa-map-marker-alt" style="font-size:1.2em;color:rgb(101,177,255);margin-right: 1vw"></i>
            <div>当前位置：报名项目</div>
        </div>
        <div class="blank"></div>
        <div class="layui-container">
            <div class="layui-carousel" id="stepForm" lay-filter="stepForm" style="margin: 0 auto;">
                <div carousel-item class="carousel">
                    <div>
                        <form class="layui-form">
                            <table class="layui-table">
                                <colgroup>
                                    <col>
                                    <col>
                                </colgroup>
                                <tbody>
                                <tr class="table-tr">
                                    <th>姓名:</th>
                                    <td>
                                        <input type="text" name="name" lay-verify="required" autocomplete="off"
                                               class="layui-input"
                                               value="<%= athlete.getName() %>" readonly>
                                    </td>
                                </tr>
                                <tr class="table-tr">
                                    <th>性别:</th>
                                    <td>
                                        <input type="text" name="gender" lay-verify="required" autocomplete="off"
                                               class="layui-input"
                                               value="<%= athlete.getGender() %>" readonly>
                                    </td>
                                </tr>
                                <tr class="table-tr">
                                    <th>出生日期:</th>
                                    <td>
                                        <input type="text" name="birthday" required lay-verify="required"
                                               placeholder="请选择出生日期"
                                               autocomplete="off" class="layui-input" id="birthday" value="<%=athlete.getBirthDate()%>">
                                    </td>
                                </tr>
                                <tr class="table-tr">
                                    <th>联系方式:</th>
                                    <td>
                                        <input type="tel" name="phone" lay-verify="required|phone"
                                               placeholder="请输入手机号"
                                               autocomplete="off" class="layui-input" value="<%=athlete.getTel()%>">
                                    </td>
                                </tr>
                                <tr class="table-tr">
                                    <th>所属组别:</th>
                                    <td>
                                        <input type="text" name="group" lay-verify="required" placeholder="请输入组别"
                                               autocomplete="off" class="layui-input" value="<%=athlete.getGroup()%>" readonly>
                                    </td>
                                </tr>
                                <tr class="table-tr">
                                    <th>所属学院:</th>
                                    <td>
                                        <input type="text" name="college" lay-verify="required" placeholder="请输入学院"
                                               autocomplete="off" class="layui-input" value=<%=athlete.getCollege()%> readonly>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="layui-form-item">
                                <div class="layui-input-block">
                                    <button class="layui-btn" lay-submit lay-filter="formStep">
                                        &emsp;下一步&emsp;
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div>
                        <form class="layui-form">
                            <table class="layui-table">
                                <colgroup>
                                    <col>
                                    <col>
                                </colgroup>
                                <tbody>
                                <tr class="table-tr">
                                    <th>报名项目:</th>
                                    <td>
                                        <select name="project" lay-verify="required" class="layui-select project" lay-search="">
                                            <option value="" hidden>请选择报名项目</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr class="table-tr">
                                    <th>历史最好成绩:</th>
                                    <td>
                                        <input type="text" name="bestgrade" lay-verify="required"
                                               placeholder="请输入此项目最好成绩"
                                               autocomplete="off" class="layui-input">
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="layui-form-item">
                                <div class="layui-input-block">
                                    <button type="button" class="layui-btn layui-btn-primary pre">上一步</button>
                                    <button class="layui-btn" lay-submit lay-filter="formStep2" id = "submit-btn">
                                        &emsp;确认报名&emsp;
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div>
                        <div style="text-align: center;margin-top: 90px;">
                            <i class="layui-icon layui-circle"
                               style="color: white;font-size:30px;font-weight:bold;background: #52C41A;padding: 20px;line-height: 80px;">&#xe605;</i>
                            <div style="font-size: 24px;color: #333;font-weight: 500;margin-top: 30px;">
                                报名成功
                            </div>
                            <div style="font-size: 14px;color: #666;margin-top: 20px;">待运动员领队审批</div>
                        </div>
                        <div style="text-align: center;margin-top: 50px;">
                            <button class="layui-btn next">继续报名</button>
                            <a href="search_signup_items.jsp"><button class="layui-btn layui-btn-primary">查看当前报名</button></a>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="text-sign">
                <h3>报名说明</h3><br>
                <h4>核对基本信息</h4>
                <p>请在运动会指定时间内完成相关的报名 </p>
                <br><h4>填写报名信息</h4>
                <p>请按照相关表单完成相关报名信息的填写，填写后等待审核</p><br><br>
            </div>
        </div>
    </div>
</div>
</body>
</html>