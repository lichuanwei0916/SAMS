<%--
  Created by IntelliJ IDEA.
  User: Li Chuanwei
  Date: 2023-05-07
  Time: 22:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>运动会管理系统</title>
    <!-- 导入图标的css -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/lib/fontawesome/css/all.css">

    <%-- 导入jquery--%>
    <script src="https://cdn.staticfile.org/jquery/1.10.2/jquery.min.js"></script>

    <!-- 导入layui的css -->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/lib/layui/css/layui.css">
    <!-- 导入layui的js -->
    <script src="${pageContext.request.contextPath}/lib/layui/layui.js"></script>

    <link rel="stylesheet" type="text/css" href="../../css/AthleteSearchSignupItems.css">
    <script src="../../javascript/AthleteSearchSignupItems.js"></script>

</head>
<body>
<%-- 检查是否已经登录，如果未登录则重定向到登录页面 --%>
<%
    session = request.getSession(false);
    if (session == null || session.getAttribute("user") == null) {
        response.sendRedirect("../unauthorized.jsp");
    }
%>
<%--导航栏--%>
<%@ include file="../headbar.jsp" %>

<div class="main">
    <div class="left">
        <jsp:include page="AthleteIndexMenu.jsp"/>
    </div>
    <div class="right">
        <div class="now-place">
            <i class="fas fa-map-marker-alt" style="font-size:1.2em;color:rgb(101,177,255);margin-right: 1vw"></i>
            <div>当前位置：查询我的报名信息</div>
        </div>
        <div class="blank"></div>
        <div class="layui-container">
            <form class="layui-form layui-form-pane search-item" lay-filter="searchForm">
                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">项目名称</label>
                        <div class="layui-input-inline">
                            <select name="project" lay-filter="project" lay-search="" class = "project">
                                <option value="">请选择项目名称</option>
                            </select>
                        </div>
                    </div>
                    <div class="layui-inline">
                        <label class="layui-form-label">审核状态</label>
                        <div class="layui-input-inline">
                            <select name="auditStatus" lay-filter="audit-status">
                                <option value="">请选择状态</option>
                                <option value="0">待审核</option>
                                <option value="2">已通过</option>
                                <option value="1">未通过</option>
                            </select>
                        </div>
                    </div>
                    <div class="layui-inline">
                        <button class="layui-btn" lay-submit lay-filter="search"><i
                                class="layui-icon layui-icon-search" id = "search"></i>搜索
                        </button>
                    </div>
                    <div class="layui-inline">
                        <button type="reset" class="layui-btn layui-btn-primary"><i
                                class="layui-icon layui-icon-refresh"></i>重置
                        </button>
                    </div>
                </div>
            </form>
            <table id="dataTable" lay-filter="dataTable"></table>
        </div>
    </div>
</div>
</body>
<script type="text\html" id="currentTableBar">
    <a class="layui-btn layui-btn-normal layui-btn-xs data-count-edit" lay-event="edit">编辑</a>
    <a class="layui-btn layui-btn-xs layui-btn-danger data-count-delete" lay-event="delete">删除</a>
</script>
</html>
