<%--
  Created by IntelliJ IDEA.
  User: Li Chuanwei
  Date: 2023-05-09
  Time: 14:18
  To change this template use File | Settings | File Templates.
  成绩申诉模块页面
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>运动会管理系统</title>
    <!-- 导入图标的css -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/lib/fontawesome/css/all.css">

    <%-- 导入jquery--%>
    <script src="https://cdn.staticfile.org/jquery/1.10.2/jquery.min.js"></script>

    <!-- 导入layui的css -->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/lib/layui/css/layui.css">
    <!-- 导入layui的js -->
    <script src="${pageContext.request.contextPath}/lib/layui/layui.js"></script>

    <link rel="stylesheet" type="text/css" href="../../css/AthleteAppealGrade.css">
    <script src="../../javascript/AthleteAppealGrade.js"></script>

</head>
<body>
<%-- 检查是否已经登录，如果未登录则重定向到登录页面 --%>
<%
    session = request.getSession(false);
    if (session == null || session.getAttribute("user") == null) {
        response.sendRedirect("../unauthorized.jsp");
    }
%>
<%--导航栏--%>
<%@ include file="../headbar.jsp" %>

<div class="main">
    <div class="left">
        <jsp:include page="AthleteIndexMenu.jsp"/>
    </div>
    <div class="right">
        <div class="now-place">
            <i class="fas fa-map-marker-alt" style="font-size:1.2em;color:rgb(101,177,255);margin-right: 1vw"></i>
            <div>当前位置：成绩申诉</div>
        </div>
        <div class="blank"></div>
        <div class="layui-container">
            <form class="layui-form" action="">
                <div class="layui-form-item">
                    <label class="layui-form-label">参赛项目</label>
                    <div class="layui-input-block">
                        <input type="text" name="project" required lay-verify="required" placeholder="请输入参赛项目"
                               autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">成绩异议</label>
                    <div class="layui-input-block">
                        <textarea name="appeal" class="layui-textarea" placeholder="请描述成绩异议"></textarea>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">证明材料</label>
                    <div class="layui-upload">
                        <button type="button" class="layui-btn layui-btn-normal" id="ID-upload-demo-files">选择多文件
                        </button>
                        <button type="button" class="layui-btn" id="ID-upload-demo-files-action">开始上传</button>
                        <div class="layui-upload-list">
                            <table class="layui-table">
                                <colgroup>
                                    <col style="min-width: 100px;">
                                    <col width="150">
                                    <col width="260">
                                    <col width="150">
                                </colgroup>
                                <thead>
                                <th>文件名</th>
                                <th>大小</th>
                                <th>上传进度</th>
                                <th>操作</th>
                                </thead>
                                <tbody id="ID-upload-demo-files-list"></tbody>
                            </table>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <button class="layui-btn" lay-submit lay-filter="formDemo">提交</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

    </div>
</div>

</body>
</html>
