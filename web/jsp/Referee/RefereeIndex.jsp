<%--
  Created by IntelliJ IDEA.
  User: Li Chuanwei
  Date: 2023-05-29
  Time: 16:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>运动会管理系统</title>
    <!-- 导入图标的css -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/lib/fontawesome/css/all.css">

    <%-- 导入jquery--%>
    <script src="https://cdn.staticfile.org/jquery/1.10.2/jquery.min.js"></script>

    <!-- 导入layui的css -->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/lib/layui/css/layui.css">
    <!-- 导入layui的js -->
    <script src="${pageContext.request.contextPath}/lib/layui/layui.js"></script>

    <link rel="stylesheet" type="text/css" href="../../css/RefereeIndex.css">

</head>
<body>
<%-- 检查是否已经登录，如果未登录则重定向到登录页面 --%>
<%
    session = request.getSession(false);
    if (session == null || session.getAttribute("user") == null ) {
        response.sendRedirect("../unauthorized.jsp");
    }
%>
<%--导航栏--%>
<%@ include file="../headbar.jsp" %>

<div class = "main">
    <div class = "left">
        <jsp:include page="RefereeIndexMenu.jsp" />
    </div>

    <!-- 右侧内容区域 -->
    <div class="right">
        <div class="now-place">
            <i class="fas fa-map-marker-alt" style="font-size:1.2em;color:rgb(101,177,255);margin-right: 1vw"></i>
            <div>当前位置：个人首页</div>
        </div>
        <div class="blank"></div>
        <div class="layui-container">
            <blockquote class="layui-elem-quote layui-text">
                欢迎来到运动会信息管理系统！
            </blockquote>
            <!-- 公告栏 -->
            <fieldset class="layui-elem-field layui-field-title">
                <legend>公告栏</legend>
            </fieldset>
            <table class="layui-table" lay-skin="line">
                <colgroup>
                    <col width="100">
                    <col>
                    <col width="200">
                </colgroup>
                <thead>
                <tr>
                    <th>序号</th>
                    <th>标题</th>
                    <th>发布时间</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>1</td>
                    <td>运动会时间调整通知</td>
                    <td>2023-05-09 09:00</td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>报名截止日期延长通知</td>
                    <td>2023-05-08 15:30</td>
                </tr>
                </tbody>
            </table>
            <a href="./" class="layui-btn layui-btn-lg layui-btn-radius layui-btn-normal">报名项目</a>
            <a href="./" class="layui-btn layui-btn-lg layui-btn-radius layui-btn-normal">项目规则</a>
            <%--      <a href="AthleteSetPassword.jsp" class="layui-btn layui-btn-lg layui-btn-radius layui-btn-normal">修改密码</a>--%>
        </div>
    </div>
</div>
</body>
</html>