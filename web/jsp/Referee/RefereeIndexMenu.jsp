<%--
  Created by IntelliJ IDEA.
  User: Li Chuanwei
  Date: 2023-05-29
  Time: 16:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="modle.Referee" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    Referee referee = new Referee();
    session = request.getSession(false);
    if (session == null || session.getAttribute("user") == null) {
        response.sendRedirect("../unauthorized.jsp");
    }
    else{
        referee = (Referee) session.getAttribute("role");
        if(referee == null){
            referee = new Referee();
            response.sendRedirect("../unerror.jsp");
        }
    }
%>
<link rel="stylesheet" type="text/css" href="../../css/RefereeIndexMenu.css">
<script src="../../javascript/AccordionMenu.js" defer="true"></script>
<script src="../../javascript/RefereeIndexMenu.js" defer="true"></script>
<div class="left_user">
    <img class="left_user-avatar" src="../../img/boy.png" alt="裁判长">
    <span class="left_user-name"><%=referee.getName()%>(裁判长)</span>
</div>
<ul class="menu_list">
    <li>
        <%--<dl>标签来创建一个术语列表--%>
        <dl class="menu_title">
            <a href="RefereeIndex.jsp">
                <i class="fas fa-home" style="font-size:1.2em;color:white;padding-right:0.7vw"></i>
                我的首页
            </a>
        </dl>
    </li>
    <li>
        <dl class="menu_head" id = "sm_items">
            <i class="fas fa-running" style="font-size:1.3em;color:white;padding-right:1.2vw"></i>
            项目情况
        </dl>
        <dt class="menu_body">
            <a href="RefereeProject.jsp" id = "RefereeProject">裁判项目</a>
            <a href="./" id = "search_signup_items">XXXX</a>
            <a href="./" id = "rules">XXXX</a>
        </dt>
    </li>
    <li>
        <dl class="menu_head" id = "my_grades">
            <i class="fas fa-trophy" style="font-size:1.2em;color:white;padding-right:0.7vw"></i>
            我的成绩
        </dl>
        <dt class="menu_body">
            <a href="./" id = "query_grades">XXXX</a>
            <a href="./" id = "appeal_grades">XXXX</a>
        </dt>
    </li>
</ul>
