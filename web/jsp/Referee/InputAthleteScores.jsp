<%--
  Created by IntelliJ IDEA.
  User: Li Chuanwei
  Date: 2023-05-30
  Time: 1:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" type="text/css" href="../../css/InputAthleteScores.css">
<script src="../../javascript/InputAthleteScores.js"></script>
<div class="input-container">
    <input type="hidden" value="<%= request.getParameter("ci_id")%>" id = "ci_id" name = "ci_id">
    <form class="layui-form layui-form-pane search-group" lay-filter="searchGroupForm">
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">运动员学号</label>
                <div class="input-part">
                    <input type="text" name="athleteNo" placeholder="请输入学号" class="layui-input" >
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">成绩状态</label>
                <div class="input-part">
                    <select name="status" lay-filter="status" class = "status">
                        <option value="">请选择项目状态</option>
                        <option value="1">已录入成绩</option>
                        <option value="2">未录入成绩</option>
                        <option value="3">未检录</option>
                    </select>
                </div>
            </div>
            <div class="layui-inline">
                <button class="layui-btn search" lay-submit lay-filter="searchGroup"><i
                        class="layui-icon layui-icon-search" id = "searchGroup"></i>搜索
                </button>
            </div>
            <div class="layui-inline">
                <button type="reset" class="layui-btn layui-btn-primary reset"><i
                        class="layui-icon layui-icon-refresh"></i>重置
                </button>
            </div>
        </div>
    </form>
    <div class="inputScoresTable">
        <table id="inputScoresTable" lay-filter="inputScoresTable"></table>
    </div>
</div>
<script type="text/html" id="InputScoreOperationBar">
    {{#  if(d.recordingStatus === "已检录") { }}
    <a class="layui-btn layui-btn-xs" style="margin-top: 6%" lay-event="inputScore">录入</a>
    {{# } else if(d.recordingStatus === "未检录") { }}
    <a class="layui-btn layui-btn-xs layui-btn-disabled" style="margin-top: 6%"  lay-event="NotStart">未检录无法录入</a>
    {{# } }}
</script>
