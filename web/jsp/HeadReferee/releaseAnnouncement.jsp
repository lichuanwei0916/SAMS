<%--
  Created by IntelliJ IDEA.
  User: Li Chuanwei
  Date: 2023-05-28
  Time: 2:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>运动会管理系统</title>
  <!-- 导入图标的css -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/lib/fontawesome/css/all.css">

  <%-- 导入jquery--%>
  <script src="https://cdn.staticfile.org/jquery/1.10.2/jquery.min.js"></script>

  <!-- 导入layui的css -->
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/lib/layui/css/layui.css">
  <!-- 导入layui的js -->
  <script src="${pageContext.request.contextPath}/lib/layui/layui.js"></script>

  <link href="../../lib/wangEditor/style.css" rel="stylesheet">
  <script src="../../lib/wangEditor/index.js"></script>

  <link rel="stylesheet" type="text/css" href="../../css/HeadRefereeReleaseAnnouncement.css">
  <script src="../../javascript/HeadRefereeReleaseAnnouncement.js" defer = "true"></script>
</head>
<body>
<%-- 检查是否已经登录，如果未登录则重定向到登录页面 --%>
<%
  session = request.getSession(false);
  if (session == null || session.getAttribute("user") == null) {
    response.sendRedirect("../unauthorized.jsp");
  }
%>

<%--导航栏--%>
<%@ include file="../headbar.jsp" %>

<div class="main">
  <div class="left">
    <jsp:include page="HeadRefereeIndexMenu.jsp" />
  </div>
  <div class="right">
    <div class="now-place">
      <i class="fas fa-map-marker-alt" style="font-size:1.2em;color:rgb(101,177,255);margin-right: 1vw"></i>
      <div>当前位置：发布公告</div>
    </div>
    <div class="blank"></div>
    <div class="layui-container">
      <div>
        <h2>发 布 公 告</h2>
      </div>
      <div>
        <label class="layui-form-label">公告标题</label>
        <input type="text" name="title" lay-verify="required"
               placeholder="请输入标题" autocomplete="off" class="layui-input">
        <label class="layui-form-label">公告内容</label>
      </div>
      <div>
        <div id="editor—wrapper">
          <div id="toolbar-container"><!-- 工具栏 --></div>
          <div id="editor-container"><!-- 编辑器 --></div>
        </div>
      </div>
      <div class="button-container">
          <button type="button" class="layui-btn layui-btn-normal" id = "submit">发布</button>
          <button type="button" class="layui-btn layui-btn-normal" id = "view">预览</button>
          <button type="button" class="layui-btn layui-btn-normal" id = "reset">清空</button>
      </div>
    </div>
  </div>
</div>
</body>
</html>
