<%--
  Created by IntelliJ IDEA.
  User: Li Chuanwei
  Date: 2023-05-15
  Time: 0:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="modle.HeadReferee" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
  HeadReferee headReferee = new HeadReferee();
  session = request.getSession(false);
  if (session == null || session.getAttribute("user") == null) {
    response.sendRedirect("../unauthorized.jsp");
  }
  else{
    headReferee = (HeadReferee) session.getAttribute("role");
    if(headReferee == null){
      headReferee = new HeadReferee();
      response.sendRedirect("../unerror.jsp");
    }
  }
%>
<link rel="stylesheet" type="text/css" href="../../css/HeadRefereeIndexMenu.css">
<script src="../../javascript/AccordionMenu.js" defer="true"></script>
<script src="../../javascript/HeadRefereeIndexMenu.js" defer="true"></script>
<div class="left_user">
  <img class="left_user-avatar" src="../../img/boy.png" alt="总裁判长">
  <span class="left_user-name"><%=headReferee.getName()%>(总裁判长)</span>
</div>
<ul class="menu_list">
  <li>
    <%--<dl>标签来创建一个术语列表--%>
    <dl class="menu_title">
      <a href="HeadRefereeIndex.jsp">
        <i class="fas fa-home" style="font-size:1.2em;color:white;padding-right:0.7vw"></i>
        我的首页
      </a>
    </dl>
  </li>
  <li>
    <dl class="menu_head" id = "sm_items">
      <i class="fas fa-running" style="font-size:1.3em;color:white;padding-right:1.2vw"></i>
      运动会项目
    </dl>
    <dt class="menu_body">
      <a href="reviewItems.jsp" id = "reviewItems">查看项目</a>
      <a href="arrangeReferees.jsp" id = "arrangeReferees">裁判安排</a>
      <a href="./" id = "rules">XXXX</a>
    </dt>
  </li>
  <li>
    <dl class="menu_head" id = "sportMeeting">
      <i class="fas fa-trophy" style="font-size:1.2em;color:white;padding-right:0.7vw"></i>
      运动会设置
    </dl>
    <dt class="menu_body">
      <a href="sportMeetingSchedule.jsp" id = "sportMeetingSchedule">运动会日程</a>
      <a href="releaseAnnouncement.jsp" id = "releaseAnnouncement">发布公告</a>
    </dt>
  </li>
</ul>