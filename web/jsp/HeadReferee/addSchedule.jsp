<%--
  Created by IntelliJ IDEA.
  User: Li Chuanwei
  Date: 2023-05-29
  Time: 0:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/addSchedule.css">
<script src="../../javascript/addSchedule.js"></script>
<div class="addScheduleContainer">
    <form class="layui-form" action="">
        <table class="layui-table" id = "addTable">
            <thead>
            <tr>
                <th colspan="4">比赛日程信息</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class = "text">比赛序号</td>
                <td><input type="text" name="no" lay-verify="" placeholder="系统自动生成，无需填写" autocomplete="off" class="layui-input" readonly></td>
                <td class = "text">比赛项目</td>
                <td><select name="project" id = "project" lay-filter="project" lay-verify="required" class="layui-select" lay-search>
                    <option value=""></option>
                </select></td>
            </tr>
            <tr>
                <td class = "text">比赛时间</td>
                <td><input type="text" class="layui-input" id="datetime" placeholder="yyyy-MM-dd HH:mm:ss" name = "datetime"></td>
                <td class = "text">比赛地点</td>
                <td><input type="text" name="venue" lay-verify="required" placeholder="请输入项目地点" autocomplete="off" class="layui-input"></td>
            </tr>
            <tr>
                <td class = "text">项目属性</td>
                <td><select name="projectAttr" lay-filter="projectAttr" lay-verify="" class="layui-select" disabled style="color:black;">
                    <option value="">系统自动匹配，无需选择</option>
                    <option value="0">个人赛</option>
                    <option value="1">团体赛</option>
                </select></td>
                <td class = "text">比赛性质</td>
                <td>
                    <select name="nature" lay-filter="nature" lay-verify="required" class="layui-select">
                        <option value=""></option>
                        <option value="1">预赛</option>
                        <option value="2">决赛</option>
                        <option value="3">预决赛</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td class = "text">项目每组人数</td>
                <td><input type="text" name="peopleLimit" lay-verify="required|peopleLimit" placeholder="请输入项目每组人数" autocomplete="off" class="layui-input"></td>
                <td class = "text">项目组别</td>
                <td><select name="group" id = "group" lay-filter="group" lay-verify="required" class="layui-select" lay-search>
                    <option value=""></option>
                </select></td>
            </tr>
            </tbody>
        </table>
        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn submit" lay-submit lay-filter="addForm-submit">提交</button>
                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
            </div>
        </div>
    </form>
</div>
