<%--
  Created by IntelliJ IDEA.
  User: 倪梓鳌
  Date: 2023-05-14
  Time: 17:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/updateItem.css">
<!-- HTML结构部分 -->
<div class="layui-container updateItemContainer">
    <form class="layui-form" action="">
        <div class="layui-form-item">
            <label class="layui-form-label">项目编号</label>
            <div class="layui-input-block">
                <input type="text" name="projectNo" readonly="readonly" lay-verify="required"
                       placeholder="请输入项目编号" autocomplete="off" class="layui-input"
                       value="<%= request.getParameter("id")%>"><!-- 项目编号在这里设置默认值 -->
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">项目名称</label>
            <div class="layui-input-block">
                <input type="text" name="projectName" lay-verify="required"
                       placeholder="请输入项目名称" autocomplete="off" class="layui-input"
                       value="<%= request.getParameter("name")%>">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">项目属性</label>
            <div class="layui-input-block">
                <select name="projectAttr" lay-filter="projectAttr" lay-verify="required" class="layui-select">
                    <option value=""></option>
                    <option value="0" <% if("0".equals(request.getParameter("itemProperty"))){ %> selected <% } %>>个人赛</option>
                    <option value="1" <% if("1".equals(request.getParameter("itemProperty"))){ %> selected <% } %>>团体赛</option>
                </select>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">项目性质</label>
            <div class="layui-input-block">
                <select name="competitionNature" lay-filter="competitionNature" lay-verify="required" class="layui-select">
                    <option value=""></option>
                    <option value="0" <% if("0".equals(request.getParameter("rateProperty"))){ %> selected <% } %>>预赛和决赛</option>
                    <option value="1" <% if("1".equals(request.getParameter("rateProperty"))){ %> selected <% } %>>决赛</option>
                </select>
            </div>
        </div>
        <div class="layui-form-item layui-form-text">
            <label class="layui-form-label">项目描述</label>
            <div class="layui-input-block">
                <textarea name="projectDesc" placeholder="请输入项目描述"
                          class="layui-textarea"><%=request.getParameter("text")%></textarea>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn" lay-submit lay-filter="updateForm-submit">提交</button>
                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
            </div>
        </div>
    </form>
</div>
<script>
    layui.use(['table', 'form'], function(){
        var table = layui.table;
        var form = layui.form;
        // 相关操作
        form.render(); // 渲染全部表单

        form.on('submit(updateForm-submit)', function (data) {
            $.ajax({
                url: '../../UpdateItemInformation',
                data: {
                    id : data.field.projectNo,
                    name : data.field.projectName,
                    category : data.field.projectAttr,
                    isFinals : data.field.competitionNature,
                    introduction : data.field.projectDesc
                },
                method: 'POST',
                dataType: 'json',
                success: function (result) {
                    if (result.code === 200) { // 修改成功
                        layer.msg('更新比赛项目成功');
                    } else {
                        layer.msg(result.msg || '更新比赛项目失败');
                    }
                    layer.closeAll('page');

                    $.ajax({
                        url: '../../SearchItemInformation',
                        method: 'POST',
                        dataType: 'json',
                        success: function (res) {
                            table.reload('dataTable',{
                                page: {
                                    curr: 1 // 重新从第 1 页开始
                                },
                                data: res.data
                            });
                        },
                        error: function (err) {
                            layer.msg( '查询数据时发生错误，请稍后再试！');
                        }
                    });
                },
                error: function (err) {
                    layer.msg( '查询数据时发生错误，请稍后再试！');
                }
            });

            return false;
        });
    });
</script>
