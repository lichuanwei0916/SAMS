<%--
  Created by IntelliJ IDEA.
  User: Li Chuanwei
  Date: 2023-06-02
  Time: 13:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" type="text/css" href="../../css/editReferees.css">
<script src="../../javascript/editReferees.js"></script>
<div class="input-container">
    <input type="hidden" value="<%= request.getParameter("ci_id")%>" id = "ci_id" name = "ci_id">
    <button type="button" class="layui-btn layui-btn-normal addReferee" id = "addReferee">新增裁判</button>
    <div class="editRefereesTable">
        <table id="editRefereesTable" lay-filter="editRefereesTable"></table>
    </div>
</div>
<script type="text/html" id="editRefereesOperationBar">
    <a class="layui-btn layui-btn-xs layui-btn-danger" style="margin-top: 6%" lay-event="delete">删除</a>
</script>
