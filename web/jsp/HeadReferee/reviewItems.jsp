<%--
  Created by IntelliJ IDEA.
  User: Ni Ziao, Li Chuanwei
  Date: 2023-05-15
  Time: 1:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>运动会管理系统</title>
  <!-- 导入图标的css -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/lib/fontawesome/css/all.css">

  <%-- 导入jquery--%>
  <script src="https://cdn.staticfile.org/jquery/1.10.2/jquery.min.js"></script>

  <!-- 导入layui的css -->
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/lib/layui/css/layui.css">
  <!-- 导入layui的js -->
  <script src="${pageContext.request.contextPath}/lib/layui/layui.js"></script>

  <link rel="stylesheet" type="text/css" href="../../css/HeadRefereeReviewItems.css">
  <script src="../../javascript/HeadRefereeReviewItems.js"></script>
</head>
<body>
<%-- 检查是否已经登录，如果未登录则重定向到登录页面 --%>
<%
  session = request.getSession(false);
  if (session == null || session.getAttribute("user") == null) {
    response.sendRedirect("../unauthorized.jsp");
  }
%>

<%--导航栏--%>
<%@ include file="../headbar.jsp" %>

<div class="main">
  <div class="left">
    <jsp:include page="HeadRefereeIndexMenu.jsp" />
  </div>
  <div class="right">
    <div class="now-place">
      <i class="fas fa-map-marker-alt" style="font-size:1.2em;color:rgb(101,177,255);margin-right: 1vw"></i>
      <div>当前位置：查询运动会比赛项目</div>
    </div>
    <div class="blank"></div>
    <div class="layui-container">
      <div class="buttonContainer">
        <button type="button" class="layui-btn layui-btn-radius addItem">增加项目</button>
        <button type="button" class="layui-btn layui-btn-radius displayItem">显示全部</button>
      </div>
      <table id="dataTable" lay-filter="dataTable"></table>
    </div>
  </div>
</div>
</body>
<script type="text\html" id="operationBar">
    <a class="layui-btn layui-btn-normal layui-btn-xs data-count-edit" lay-event="edit">编辑</a>
    <a class="layui-btn layui-btn-xs layui-btn-danger data-count-delete" lay-event="delete">删除</a>
</script>
</html>
