<%--
  Created by IntelliJ IDEA.
  User: Li Chuanwei
  Date: 2023-05-27
  Time: 15:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/updateSchedule.css">
<script src="../../javascript/updateSchedule.js"></script>
<div class="updateScheduleContainer">
    <form class="layui-form" action="">
        <table class="layui-table" id = "updateTable">
            <thead>
            <tr>
                <th colspan="4">比赛日程信息</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class = "text">比赛序号</td>
                <td><input type="text" name="no" lay-verify="required" placeholder="比赛序号" autocomplete="off" class="layui-input"
                           value="<%= request.getParameter("id")%>" readonly></td>
                <td class = "text">比赛项目<input type="hidden" name = "project" value="<%= request.getParameter("projectId")%>"></td>
                <td><input type="text" name="itemName" lay-verify="required" placeholder="项目名字" autocomplete="off" class="layui-input"
                           value="<%= request.getParameter("name")%>" readonly></td>
            </tr>
            <tr>
                <td class = "text">比赛时间</td>
                <td><input type="text" name = "datetime" class="layui-input" id="datetime" placeholder="yyyy-MM-dd HH:mm:ss"
                           value="<%= request.getParameter("time")%>"></td>
                <td class = "text">比赛地点</td>
                <td><input type="text" name="venue" lay-verify="required" placeholder="请输入项目地点" autocomplete="off" class="layui-input"
                           value="<%= request.getParameter("venue")%>"></td>
            </tr>
            <tr>
                <td class = "text">项目属性</td>
                <td><select name="projectAttr" lay-filter="projectAttr" lay-verify="required" class="layui-select" disabled style="color:black;">
                    <option value=""></option>
                    <option value="0" <% if("0".equals(request.getParameter("itemProperty"))){ %> selected <% } %>>个人赛</option>
                    <option value="1" <% if("1".equals(request.getParameter("itemProperty"))){ %> selected <% } %>>团体赛</option>
                </select></td>
                <td class = "text">比赛性质</td>
                <td>
                    <select name="nature" lay-filter="nature" lay-verify="required" class="layui-select">
                        <option value=""></option>
                        <option value="1" <% if("1".equals(request.getParameter("nature"))){ %> selected <% } %>>预赛</option>
                        <option value="2" <% if("2".equals(request.getParameter("nature"))){ %> selected <% } %>>决赛</option>
                        <option value="3" <% if("3".equals(request.getParameter("nature"))){ %> selected <% } %>>预决赛</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td class = "text">项目每组人数</td>
                <td><input type="text" name="peopleLimit" lay-verify="required|peopleLimit" placeholder="请输入项目每组人数" autocomplete="off" class="layui-input"
                           value="<%= request.getParameter("peopleLimit")%>"></td>
                <td class = "text">项目组别</td>
                <td><select name="group" id = "group" lay-filter="group" lay-verify="required" class="layui-select" lay-search>
                    <option value=""></option>
                </select></td>
            </tr>
            </tbody>
        </table>
        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn submit" lay-submit lay-filter="updateForm-submit">提交</button>
                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
            </div>
        </div>
    </form>
</div>
<script>
    $(function() {
        // 获取 peopleLimit 参数的值
        var groupID = '<%= request.getParameter("groupID") %>';


        // 如果 peopleLimit 不为空，则设置选择框的默认选项
        if (groupID) {
            setTimeout(function() {
                $('#group option').each(function() {
                    if ($(this).val().toString() === groupID) {
                        $(this).prop('selected', true);
                        return false;
                    }
                });
                // 重新渲染表单元素
                layui.form.render();
            }, 10); // 延迟 0.01 秒执行
        }
    });
</script>
