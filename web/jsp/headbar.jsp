<%--
  Created by IntelliJ IDEA.
  User: Li Chuanwei
  Date: 2023-04-23
  Time: 21:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" type="text/css" href="../../css/headbar.css">
<script src="../../javascript/headbar.js"></script>
<%--相对于加入的路径--%>
<header>
    <img src="../../img/school.png" alt="school"/>
    <p class="brand">运动会信息管理系统</p>
    <div href ="" class = "head_button" id = "lock">
        <i class="fas fa-lock" style="font-size:1.2em;color:white"></i>
        <p>锁屏</p>
    </div>

    <a href ="" class = "head_button">
        <i class="fas fa-envelope" style="font-size:1.2em;color:white"></i>
        <p>操作中心</p>
    </a>

    <a href =""  class = "head_button">
        <i class="fa fa-user" style="font-size:1.2em;color:white"></i>
        <p>个人中心</p>
    </a>

    <a href="../../ClearUserSession" class = "head_button">
        <i class="fas fa-sign-out-alt" style="font-size:1.2em;color:white"></i>
        <p>退出账号</p>
    </a>
</header>