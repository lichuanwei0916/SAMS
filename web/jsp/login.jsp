<%--
  Created by IntelliJ IDEA.
  User: Li Chuanwei
  Date: 2023-03-27
  Time: 0:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%--    Sports Activities Management System--%>
    <title>运动会管理系统</title>
    <!-- 导入图标的css -->
    <link rel="stylesheet" type="text/css" href="https://cdn.staticfile.org/font-awesome/4.7.0/css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="../css/login.css">
    <script src="https://cdn.staticfile.org/jquery/1.10.2/jquery.min.js"></script>
    <!-- 导入时间的js -->
    <script src="../javascript/time.js" defer="true"></script>
    <!-- 导入图片切换的js -->
    <script src="../javascript/CarouselIMG.js" defer="true"></script>
    <!-- 导入切换文本框的文字的js -->
    <script src="../javascript/InputColor.js" defer="true"></script>
    <script src="../javascript/login.js" defer="true"></script>
</head>
<body>
    <header>
        <img src="../img/school.png" alt="school"/>
        <div class="logo">
            <h6>Nanchang Hangkong University<br>
            Sports Meeting Management System</h6>
        </div>
        <div class="line"></div>
        <h2 class="brand">南昌航空大学运动会管理系统</h2>
        <div class="time">
            <h6 class="time"></h6>
        </div>
    </header>

    <div class = "main">
        <!-- 左侧轮播图 -->
        <div class = "left">
            <div class="left_container">
                <!-- 放图片-->
                <div class = "carousel">
                    <a href="#">
                        <!-- 仅放一张图片，通过JS来改变图片地址即可，也可以放多个a标签，在每个a标签中放图片-->
                        <img id = "picture" src="../img/img1.png" />
                    </a>
                </div>
                <!-- 放底部圆点-->
                <div id="selector">
                    <span class="dot"></span>
                    <span class="dot"></span>
                    <span class="dot"></span>
                    <span class="dot"></span>
                </div>
                <!-- 左箭头-->
                <div id="left">&lt;</div>
                <!-- 右箭头-->
                <div id="right">&gt;</div>
            </div>
        </div>

        <!-- 右侧登录框 -->
        <div class="wrapper">
            <form class="login" action="../login" method="post">
                <p class="title">用户登录</p>

                <select name="type" id="selection" class = "input"  autofocus required>
                    <option id = "default" disabled selected hidden>请选择登录身份</option>
                    <option value = "1">运动员</option>
                    <option value = "2">运动员领队</option>
                    <option value = "3">检录员</option>
                    <option value = "4">裁判员</option>
                    <option value = "5">总裁判长</option>
                    <option value = "6">系统管理员</option>
                </select>
                <i class="fa fa-id-card"></i>


                <input type="text" name="acc" placeholder="请输入账号" id="inacc" class = "input" required/>
                <i class="fa fa-user"></i>


                <input type="password" name="pwd" placeholder="请输入密码" id="inpwd" class = "input" required/>
                <i class="fa fa-key"></i>
                <div>
                    <a href="#">Forgot your password?</a>
                </div>
                <button id="btn">
                    <i class="spinner"></i>
                    <span class="state">Log in</span>
                </button>
            </form>
        </div>
    </div>

    <footer id="foot">版权所有© 2023运动会管理系统-212061-01</footer>
</body>
</html>