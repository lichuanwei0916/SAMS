<%--
  Created by IntelliJ IDEA.
  User: Li Chuanwei
  Date: 2023-05-04
  Time: 23:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" type="text/css" href="../../css/SetPassword.css">
<script src="../../javascript/SetPassword.js" defer="true"></script>
<div class="layui-card">
    <div class="layui-card-header" style="color: #1e9fff;text-align: center;font-size:2em;margin-bottom: 1vh">修改密码</div>
    <div class="layui-card-body">
        <form class="layui-form" lay-filter="password-form">
            <div class="layui-form-item">
                <label class="layui-form-label">旧密码</label>
                <div class="layui-input-inline">
                    <input type="password" name="oldPassword" required lay-verify="required" placeholder="请输入旧密码"
                           autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">新密码</label>
                <div class="layui-input-inline">
                    <input type="password" name="newPassword" required lay-verify="required" placeholder="请输入新密码"
                           autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">确认新密码</label>
                <div class="layui-input-inline">
                    <input type="password" name="confirmPassword" required lay-verify="required|confirm"
                           placeholder="请再次输入新密码" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">验证码</label>
                <div class="layui-row">
                    <div class="layui-input-inline">
                        <div class="layui-input-wrap">
                            <div class="layui-input-prefix">
                                <i class="layui-icon layui-icon-vercode"></i>
                            </div>
                            <input type="text" id = "captcha-input" name="captcha" value="" lay-verify="required" placeholder="验证码" lay-reqtext="请填写验证码" autocomplete="off" class="layui-input" lay-affix="clear">
                        </div>
                    </div>
                    <div class="layui-input-inline">
                        <div>
                            <img id="captcha-image" src="https://www.oschina.net/action/user/captcha" onclick="this.src='https://www.oschina.net/action/user/captcha?t='+ new Date().getTime();">
                        </div>
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <a class="layui-btn layui-btn-primary my-green-button" href="javascript:history.back(-1);">返回</a>
                    <button class="layui-btn layui-btn-primary my-yello-button" type="reset">重置</button>
                    <button class="layui-btn my-blue-button" lay-submit lay-filter="submit-password-form">提交</button>
                </div>
            </div>
        </form>
    </div>
</div>