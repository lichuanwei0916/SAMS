<%--
  Created by IntelliJ IDEA.
  User: Li Chuanwei
  Date: 2023-04-19
  Time: 19:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>运动会管理系统</title>
    <link rel="stylesheet" type="text/css" href="../css/unauthorized.css">
</head>
<body>
    <div>
        <div class="unauthorized">
            <div class="unauthorized-content">
                <div class="unauthorized-picture"></div>
                <div class="unauthorized-text">抱歉，登录用户与角色不对应，请联系管理员更改！</div>
                <a href="login.jsp">
                    <button class="unauthorized-button" type="button">
                        <span>重新登录</span>
                    </button>
                </a>
        </div>
    </div>
</div>
</body>
</html>