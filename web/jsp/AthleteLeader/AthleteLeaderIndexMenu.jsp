<%--
  Created by IntelliJ IDEA.
  User: Li Chuanwei
  Date: 2023-05-15
  Time: 0:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="modle.AthleteLeader" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
  AthleteLeader athleteLeader = new AthleteLeader();
  session = request.getSession(false);
  if (session == null || session.getAttribute("user") == null) {
    response.sendRedirect("../unauthorized.jsp");
  }
  else{
    athleteLeader = (AthleteLeader) session.getAttribute("role");
    if(athleteLeader == null){
      athleteLeader = new AthleteLeader();
      response.sendRedirect("../unerror.jsp");
    }
  }
%>
<link rel="stylesheet" type="text/css" href="../../css/AthleteLeaderIndexMenu.css">
<script src="../../javascript/AccordionMenu.js" defer="true"></script>
<script src="../../javascript/AthleteLeaderIndexMenu.js" defer="true"></script>
<div class="left_user">
  <img class="left_user-avatar" src="../../img/boy.png" alt="运动员领队">
  <span class="left_user-name"><%=athleteLeader.getName()%>(运动员领队)</span>
</div>
<ul class="menu_list">
  <li>
    <%--<dl>标签来创建一个术语列表--%>
    <dl class="menu_title">
      <a href="AthleteLeaderIndex.jsp">
        <i class="fas fa-home" style="font-size:1.2em;color:white;padding-right:0.7vw"></i>
        我的首页
      </a>
    </dl>
  </li>
  <li>
    <dl class="menu_head" id = "sm_items">
      <i class="fas fa-running" style="font-size:1.3em;color:white;padding-right:1.2vw"></i>
      项目情况
    </dl>
    <dt class="menu_body">
      <a href="examineRecords.jsp" id = "examineRecords">审核报名</a>
      <a href="./searchSchedule.jsp" id = "searchSchedule">查看日程</a>
    </dt>
  </li>
  <li>
    <dl class="menu_head" id = "my_grades">
      <i class="fas fa-trophy" style="font-size:1.2em;color:white;padding-right:0.7vw"></i>
      学院成绩
    </dl>
    <dt class="menu_body">
      <a href="./query_grades.jsp" id = "query_grades">成绩查询</a>
    </dt>
  </li>
</ul>