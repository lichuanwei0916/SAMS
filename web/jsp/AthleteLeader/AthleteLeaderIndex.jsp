<%--
  Created by IntelliJ IDEA.
  User: Li Chuanwei
  Date: 2023-05-15
  Time: 0:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>运动会管理系统</title>
  <!-- 导入图标的css -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/lib/fontawesome/css/all.css">

  <%-- 导入jquery--%>
  <script src="https://cdn.staticfile.org/jquery/1.10.2/jquery.min.js"></script>

  <!-- 导入layui的css -->
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/lib/layui/css/layui.css">
  <!-- 导入layui的js -->
  <script src="${pageContext.request.contextPath}/lib/layui/layui.js"></script>
  <script src="../../lib/echarts/echarts.js"></script>
  <link rel="stylesheet" type="text/css" href="../../css/AthleteLeaderIndex.css">

</head>
<body>
<%-- 检查是否已经登录，如果未登录则重定向到登录页面 --%>
<%
  session = request.getSession(false);
  if (session == null || session.getAttribute("user") == null ) {
    response.sendRedirect("../unauthorized.jsp");
  }
%>
<%--导航栏--%>
<%@ include file="../headbar.jsp" %>

<div class = "main">
  <div class = "left">
    <jsp:include page="AthleteLeaderIndexMenu.jsp" />
  </div>

  <!-- 右侧内容区域 -->
  <div class="right">
    <div class="now-place">
      <i class="fas fa-map-marker-alt" style="font-size:1.2em;color:rgb(101,177,255);margin-right: 1vw"></i>
      <div>当前位置：个人首页</div>
    </div>
    <div class="blank"></div>
    <div class="layui-container">
      <blockquote class="layui-elem-quote layui-text">
        欢迎来到运动会信息管理系统！
      </blockquote>
      <!-- 公告栏 -->
      <fieldset class="layui-elem-field layui-field-title">
        <legend>公告栏</legend>
      </fieldset>
      <table class="layui-table" lay-skin="line">
        <colgroup>
          <col width="100">
          <col>
          <col width="200">
        </colgroup>
        <thead>
        <tr>
          <th>序号</th>
          <th>标题</th>
          <th>发布时间</th>
        </tr>
        </thead>
        <tbody>
        <tr>
          <td>1</td>
          <td>运动会时间调整通知</td>
          <td>2023-05-09 09:00</td>
        </tr>
        </tbody>
      </table>
      <div class = "chart" style="display: block ruby;">
        <div id="chart" style="width: 600px;height:300px;"></div>
        <div id="chart2" style="width: 600px;height:300px;"></div>
      </div>
      <script type="text/javascript">
        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('chart'));

        // 指定图表的配置项和数据
        var option = {
          title: {
            text: ''
          },
          tooltip: {},
          legend: {
            data: ['人数']
          },
          xAxis: {
            data: ['审核通过', '审核不通过', '未审核']
          },
          yAxis: {},
          series: [
            {
              name: '人数',
              type: 'bar',
              data: [5, 20, 36]
            }
          ]
        };

        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);
      </script>
      <script type="text/javascript">
        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('chart2'));

        // 指定图表的配置项和数据
        var option = {
          title: {
            text: ''
          },
          tooltip: {
            trigger: 'item',
            formatter: "{a} <br/>{b}: {c} ({d}%)"
          },
          legend: {
            orient: 'vertical',
            x: 'left',
            data:['男','女']
          },
          series: [
            {
              name:'人数',
              type:'pie',
              radius: ['50%', '70%'],
              avoidLabelOverlap: true,
              label: {
                normal: {
                  show: false,
                  position: 'center'
                },
                emphasis: {
                  show: true,
                  textStyle: {
                    fontSize: '30',
                    fontWeight: 'bold'
                  }
                }
              },
              labelLine: {
                normal: {
                  show: false
                }
              },
              data:[
                {value:36, name:'男'},
                {value:20, name:'女'}
              ]
            }
          ]
        };

        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);
      </script>
<%--      <a href="AthleteSetPassword.jsp" class="layui-btn layui-btn-lg layui-btn-radius layui-btn-normal">修改密码</a>--%>
    </div>
  </div>
</div>
</body>
</html>