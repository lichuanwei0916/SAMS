package controller.servlets;

import dao.AthleteCompetitionInformationDao;
import dao.ItemInformationDao;
import modle.Athlete;
import modle.Referee;
import modle.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "UpdateAthleteCompetitionGradeServlet", value = "/UpdateAthleteCompetitionGrade")
public class UpdateAthleteCompetitionGradeServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String ci_id = request.getParameter("ci_id");
        String id = request.getParameter("id");
        String score = request.getParameter("score");
        int r_id;
        HttpSession session = request.getSession();
        User user = (User)session.getAttribute("user");
        String type = user.getRoletype();
        if(!type.equals("4"))
            r_id = -100;
        else{
            Referee referee = (Referee) session.getAttribute("role");
            r_id = referee.getId();
        }
        AthleteCompetitionInformationDao athleteCompetitionInformationDao = new AthleteCompetitionInformationDao();

        response.setContentType("application/json;charset=utf-8");
        PrintWriter out = response.getWriter();
        athleteCompetitionInformationDao.upDateAthleteCompetitionScore(
                Integer.parseInt(ci_id),
                Integer.parseInt(id),
                score,
                r_id);
        out.print("{\"code\": 200, \"msg\": \"\"}");
        out.flush();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
