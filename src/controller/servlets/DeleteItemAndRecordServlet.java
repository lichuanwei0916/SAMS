package controller.servlets;

import dao.ItemInformationDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "DeleteItemAndRecordServlet", value = "/DeleteItemAndRecord")
public class DeleteItemAndRecordServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id = request.getParameter("id");

        ItemInformationDao itemInformationDao = new ItemInformationDao();

        response.setContentType("application/json;charset=utf-8");
        PrintWriter out = response.getWriter();
        if(itemInformationDao.deleteItemAndRecord(Integer.parseInt(id)))
            out.print("{\"code\": 200, \"msg\": \"\"}");
        else
            out.print("{\"code\": -1, \"msg\": \"\"}");
        out.flush();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
