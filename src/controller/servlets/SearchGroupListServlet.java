package controller.servlets;

import dao.GroupInformationDao;
import modle.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "SearchGroupListServlet", value = "/SearchGroupList")
public class SearchGroupListServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        GroupInformationDao groupInformationDao = new GroupInformationDao();
        List<GroupInformation> dataList = groupInformationDao.getSearchItemInformation();

        String jsonString = ParamsUtil.objectToJsonStr(dataList);
        // 设置响应的Content-Type并将JSON字符串写入输出流
        response.setContentType("application/json;charset=utf-8");
        response.getWriter().write(jsonString);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
