package controller.servlets;

import dao.SignUpRecordDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "NotApproveSignUpRecordServlet", value = "/notApproveSignUpRecord")
public class NotApproveSignUpRecordServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json;charset=utf-8");
        PrintWriter out = response.getWriter();
        SignUpRecordDao signUpRecordDao = new SignUpRecordDao();
        //根据相关的id审核记录
        String id = request.getParameter("sr_id");

        String msg = signUpRecordDao.notApprovedSignUpRecord(Integer.parseInt(id));

        if(msg.equals("OK"))
            out.print("{\"code\": 200, \"msg\": \"\"}");
        else
            out.print("{\"code\": 100, \"msg\":\"" + msg + "\"}");

        out.flush();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
