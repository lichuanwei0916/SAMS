package controller.servlets;

import dao.ItemInformationDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "AddItemInformationServlet", value = "/AddItemInformation")
public class AddItemInformationServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String name = request.getParameter("name");
        String category = request.getParameter("category");
        String isFinals = request.getParameter("isFinals");
        String introduction = request.getParameter("introduction");

        ItemInformationDao itemInformationDao = new ItemInformationDao();

        response.setContentType("application/json;charset=utf-8");
        PrintWriter out = response.getWriter();
        if(itemInformationDao.insertItem(
                name,
                Integer.parseInt(category),
                Integer.parseInt(isFinals),
                introduction
        ))
            out.print("{\"code\": 200, \"msg\": \"\"}");
        else
            out.print("{\"code\": -1, \"msg\": \"\"}");
        out.flush();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}