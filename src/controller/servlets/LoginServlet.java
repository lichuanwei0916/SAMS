/*
Created by IntelliJ IDEA.
@author Li Chuanwei
Date: 2023-04-06
Time: 10:34
*/
package controller.servlets;

import dao.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import modle.*;

import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "LoginServlet", value = "/login")
public class LoginServlet extends HttpServlet {
    public LoginServlet() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        String path = null;//地址路径
        String type = request.getParameter("type");
        String uAcc = request.getParameter("acc");
        String uPwd = request.getParameter("pwd");
        UserDao LDao = new UserDao();
        int status = LDao.checkAccount(type, uAcc, uPwd);
        switch (status) {
            case 0:
                out.println("<h1>数据库有误！</h1>");
                break;
            case 1:
                path = "./jsp/login.jsp"+"?isSuc=0";
                response.sendRedirect(path);
                break;
            case 2:
                // 如果登录成功，则创建Session对象并将用户信息存储在其中
                HttpSession session = request.getSession();
                // 设置Session超时时间为30分钟（1800秒）
                session.setMaxInactiveInterval(1800);
                session.setAttribute("user", new User(uAcc,type));
                //分用户类别进入界面
                switch (type){
                    case "1":
                        path = "./jsp/Athlete/AthleteIndex.jsp";
                        Athlete athlete = new AthleteInformationDao().getAthleteInformation(uAcc);
                        session.setAttribute("role", athlete);
                        break;
                    case "2":
                        path = "./jsp/AthleteLeader/AthleteLeaderIndex.jsp";
                        AthleteLeader athleteLeader = new AthleteLeaderInformationDao().getAthleteLeaderInformation(uAcc);
                        session.setAttribute("role", athleteLeader);
                        break;
                    case "4":
                        path = "./jsp/Referee/RefereeIndex.jsp";
                        Referee referee = new RefereeInformationDao().getRefereeInformation(uAcc);
                        session.setAttribute("role", referee);
                        break;
                    case "5":
                        path = "./jsp/HeadReferee/HeadRefereeIndex.jsp";
                        HeadReferee headReferee = new HeadRefereeInformationDao().getHeadRefereeInformation(uAcc);
                        session.setAttribute("role", headReferee);
                        break;
                }
                response.sendRedirect(path);
                break;
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
