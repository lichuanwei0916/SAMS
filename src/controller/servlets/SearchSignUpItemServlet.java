package controller.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import dao.SignUpRecordDao;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import modle.*;

import java.io.IOException;
import java.io.Serial;
import java.util.List;

@WebServlet(name = "SearchSignUpItemServlet", value = "/searchSignUpItem")
public class SearchSignUpItemServlet extends HttpServlet {
    @Serial
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String a_athleteName = request.getParameter("a_athleteName");
        String s_college = request.getParameter("s_college");
        String s_item = request.getParameter("s_item");
        String s_status = request.getParameter("s_status");
        int a_id, college, item, status;
        String name = null;
        HttpSession session = request.getSession();
        User user = (User)session.getAttribute("user");
        String type = user.getRoletype();
        if(!type.equals("1"))
            a_id = -100;
        else{
            Athlete athlete = (Athlete)session.getAttribute("role");
            a_id = athlete.getId();
        }


        if(s_college == null || !s_college.matches("\\d+")){
            if(!type.equals("2"))
                college = -100;
            else{
                AthleteLeader athleteLeader = (AthleteLeader) session.getAttribute("role");
                college = athleteLeader.getCollegeID();
            }
        }
        else
            college = Integer.parseInt(s_college);

        if(s_item == null || !s_item.matches("\\d+"))
            item = -100;
        else
            item = Integer.parseInt(s_item);

        if(s_status == null || !s_status.matches("\\d+"))
            status = -100;
        else
            status = Integer.parseInt(s_status);

        if(a_athleteName == null || a_athleteName.equals(""))
            name = null;
        else
            name = "%" + a_athleteName + "%";


        SignUpRecordDao dao = new SignUpRecordDao();
        List<AthleteSignUpRecord> dataList = dao.getSearchSignUpRecord(a_id, college, item, status, name);

        // 将Java对象转换为JSON字符串
        ObjectMapper mapper = new ObjectMapper();
//        String jsonString = mapper.writeValueAsString(dataList);
        String code = "{\"code\": 0, \"msg\": \"\", \"count\": " + dataList.size() + ", \"data\":";
        String jsonString = ParamsUtil.objectToJsonStr(dataList);

        // 设置响应的Content-Type并将JSON字符串写入输出流
        response.setContentType("application/json;charset=utf-8");
        response.getWriter().write(code + jsonString + "}");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
