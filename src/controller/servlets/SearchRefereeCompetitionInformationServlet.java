package controller.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import dao.RefereeCompetitionInformationDao;
import modle.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.Serial;
import java.util.List;

@WebServlet(name = "SearchRefereeCompetitionInformationServlet", value = "/SearchRefereeCompetitionInformation")
public class SearchRefereeCompetitionInformationServlet extends HttpServlet {
    @Serial
    private static final long serialVersionUID = 1L;
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String Ssi_id = request.getParameter("si_id");
        String Sci_id = request.getParameter("ci_id");
        String Sg_id = request.getParameter("g_id");
        int r_id, si_id, ci_id, g_id;
        HttpSession session = request.getSession();
        User user = (User)session.getAttribute("user");
        String type = user.getRoletype();
        if(!type.equals("4"))
            r_id = -100;
        else{
            Referee referee = (Referee)session.getAttribute("role");
            r_id = referee.getId();
        }

        if(Ssi_id == null || !Ssi_id.matches("\\d+"))
            si_id = -100;
        else
            si_id = Integer.parseInt(Ssi_id);

        if(Sci_id == null || !Sci_id.matches("\\d+"))
            ci_id = -100;
        else
            ci_id = Integer.parseInt(Sci_id);

        if(Sg_id == null || !Sg_id.matches("\\d+"))
            g_id = -100;
        else
            g_id = Integer.parseInt(Sg_id);


        RefereeCompetitionInformationDao refereeCompetitionInformationDao = new RefereeCompetitionInformationDao();
        List<RefereeCompetitionInformation> dataList = refereeCompetitionInformationDao.getRefereeCompetitionInformation(r_id, si_id, ci_id, g_id);

        // 将Java对象转换为JSON字符串
        ObjectMapper mapper = new ObjectMapper();
//        String jsonString = mapper.writeValueAsString(dataList);
        String code = "{\"code\": 0, \"msg\": \"\", \"count\": " + dataList.size() + ", \"data\":";
        String jsonString = ParamsUtil.objectToJsonStr(dataList);

        // 设置响应的Content-Type并将JSON字符串写入输出流
        response.setContentType("application/json;charset=utf-8");
        response.getWriter().write(code + jsonString + "}");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
