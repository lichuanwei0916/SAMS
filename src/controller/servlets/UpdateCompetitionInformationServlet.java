package controller.servlets;

import dao.CompetitionDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "UpdateCompetitionInformationServlet", value = "/UpdateCompetitionInformation")
public class UpdateCompetitionInformationServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String ci_id = request.getParameter("ci_id");
        String venue = request.getParameter("venue");
        String nature = request.getParameter("nature");
        String peopleLimit = request.getParameter("peopleLimit");
        String group = request.getParameter("group");
        String project = request.getParameter("project");
        String time = request.getParameter("datetime");

        CompetitionDao competitionDao = new CompetitionDao();
        String msg = competitionDao.updateCompetitionInformation(
                Integer.parseInt(ci_id),
                venue,
                time,
                Integer.parseInt(project),
                Integer.parseInt(peopleLimit),
                Integer.parseInt(nature),
                Integer.parseInt(group)
                );

        response.setContentType("application/json;charset=utf-8");
        PrintWriter out = response.getWriter();
        if(msg.equals("OK"))
            out.print("{\"code\": 200, \"msg\": \"\"}");
        else
            out.print("{\"code\": 100, \"msg\":\"" + msg + "\"}");
        out.flush();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}