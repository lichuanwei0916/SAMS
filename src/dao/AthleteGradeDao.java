package dao;

import modle.AthleteCompetitionGrade;
import modle.JDBCUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class AthleteGradeDao {
    private Connection connection = null;
    public List<AthleteCompetitionGrade> getSearchAthleteCompetitionGrade(int a_id) {
        List<AthleteCompetitionGrade> list = new ArrayList<AthleteCompetitionGrade>();
        PreparedStatement pstmt = null;//声明一个PreparedStatement对象并将其初始化为null
        StringBuilder sql = new StringBuilder("CALL C_P_Search_Grade_Athlete(?)");//参数占位符
        ResultSet rs = null;
        try{
            connection = JDBCUtil.getConn();
            pstmt = connection.prepareStatement(sql.toString(),ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
            //ResultSet.TYPE_SCROLL_INSENSITIVE：游标可以前后移动，结果集对创建结果集后其他人对数据库所做的更改不敏感
            // 结果集是只读的，这是默认的并发类型
            pstmt.setInt(1, a_id);
            rs = pstmt.executeQuery();
            int num = 1;//项目序号
            while (rs.next()) {
                Calendar cal = Calendar.getInstance();
                // 获取时间戳信息
                Timestamp rtimestamp = rs.getTimestamp("CI_TIME");
                Timestamp rnewTimestamp = null;
                if(rtimestamp != null){
                    cal.setTimeInMillis(rtimestamp.getTime());
                    // 对Calendar对象进行处理，减少8个小时
                    cal.add(Calendar.HOUR_OF_DAY, -8);

                    // 使用Calendar对象创建一个新的timestamp对象
                    rnewTimestamp = new java.sql.Timestamp(cal.getTime().getTime());
                }
                AthleteCompetitionGrade athleteCompetitionGrade = new AthleteCompetitionGrade();
                athleteCompetitionGrade.setNum(num++);
                athleteCompetitionGrade.setId(rs.getInt("ID"));
                athleteCompetitionGrade.setA_name(rs.getString("A_Name"));
                athleteCompetitionGrade.setSi_name(rs.getString("SI_Name"));
                athleteCompetitionGrade.setRecordingStatus(rs.getInt("RecordingStatus"));
                athleteCompetitionGrade.setCi_time(rnewTimestamp);
                athleteCompetitionGrade.setGroup(rs.getInt("Group"));
                athleteCompetitionGrade.setRunway(rs.getInt("Runway"));
                athleteCompetitionGrade.setGrade(rs.getString("Grade"));

                list.add(athleteCompetitionGrade);
            }
            pstmt.close();
            connection.close();
        }catch (SQLException e) {
            //e.printStackTrace();
        }
        catch (NullPointerException e){
            return null;
        }finally{
            try{
                if (connection != null && (!connection.isClosed())){
                    connection.close();
                }
            }catch(SQLException e){
                //e.printStackTrace();
            }
        }
        return list;
    }
    public List<AthleteCompetitionGrade> getSearchCollegeCompetitionGrade(int c_id) {
        List<AthleteCompetitionGrade> list = new ArrayList<AthleteCompetitionGrade>();
        PreparedStatement pstmt = null;//声明一个PreparedStatement对象并将其初始化为null
        StringBuilder sql = new StringBuilder("CALL C_P_Search_Grade_College(?)");//参数占位符
        ResultSet rs = null;
        try{
            connection = JDBCUtil.getConn();
            pstmt = connection.prepareStatement(sql.toString(),ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
            //ResultSet.TYPE_SCROLL_INSENSITIVE：游标可以前后移动，结果集对创建结果集后其他人对数据库所做的更改不敏感
            // 结果集是只读的，这是默认的并发类型
            pstmt.setInt(1, c_id);
            rs = pstmt.executeQuery();
            int num = 1;//项目序号
            while (rs.next()) {
                Calendar cal = Calendar.getInstance();
                // 获取时间戳信息
                Timestamp rtimestamp = rs.getTimestamp("CI_TIME");
                Timestamp rnewTimestamp = null;
                if(rtimestamp != null){
                    cal.setTimeInMillis(rtimestamp.getTime());
                    // 对Calendar对象进行处理，减少8个小时
                    cal.add(Calendar.HOUR_OF_DAY, -8);

                    // 使用Calendar对象创建一个新的timestamp对象
                    rnewTimestamp = new java.sql.Timestamp(cal.getTime().getTime());
                }
                AthleteCompetitionGrade athleteCompetitionGrade = new AthleteCompetitionGrade();
                athleteCompetitionGrade.setNum(num++);
                athleteCompetitionGrade.setId(rs.getInt("ID"));
                athleteCompetitionGrade.setA_name(rs.getString("A_Name"));
                athleteCompetitionGrade.setSi_name(rs.getString("SI_Name"));
                athleteCompetitionGrade.setRecordingStatus(rs.getInt("RecordingStatus"));
                athleteCompetitionGrade.setCi_time(rnewTimestamp);
                athleteCompetitionGrade.setGroup(rs.getInt("Group"));
                athleteCompetitionGrade.setRunway(rs.getInt("Runway"));
                athleteCompetitionGrade.setGrade(rs.getString("Grade"));

                list.add(athleteCompetitionGrade);
            }
            pstmt.close();
            connection.close();
        }catch (SQLException e) {
            //e.printStackTrace();
        }
        catch (NullPointerException e){
            return null;
        }finally{
            try{
                if (connection != null && (!connection.isClosed())){
                    connection.close();
                }
            }catch(SQLException e){
                //e.printStackTrace();
            }
        }
        return list;
    }
}
