package dao;

import modle.Athlete;
import modle.AthleteLeader;
import modle.JDBCUtil;

import java.sql.*;

public class AthleteLeaderInformationDao {
    private Connection connection = null;
    public AthleteLeader getAthleteLeaderInformation(String account) {
        AthleteLeader athleteLeader = new AthleteLeader();
        PreparedStatement pstmt = null;//声明一个PreparedStatement对象并将其初始化为null
        String sql = "select * from v_athleteLeader_information where Account = ?";//参数占位符
        ResultSet rs = null;
        try{
            connection = JDBCUtil.getConn();
            pstmt = connection.prepareStatement(sql,ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
            //ResultSet.TYPE_SCROLL_INSENSITIVE：游标可以前后移动，结果集对创建结果集后其他人对数据库所做的更改不敏感
            // 结果集是只读的，这是默认的并发类型
            pstmt.setString(1, account);
            rs = pstmt.executeQuery();
            rs.next();
            athleteLeader.setId(rs.getInt("L_ID"));
            athleteLeader.setName(rs.getString("L_Name"));
            athleteLeader.setNo(rs.getString("L_No"));
            athleteLeader.setGender(rs.getString("L_Gender"));
            athleteLeader.setBirthDate(rs.getDate("L_Birth"));
            athleteLeader.setTel(rs.getString("L_Tel"));
            athleteLeader.setCollege(rs.getString("College"));
            athleteLeader.setCollegeID(rs.getInt("c_id"));
            pstmt.close();
            connection.close();
        }catch (SQLException e) {
            //e.printStackTrace();
            return null;
        }
        catch (NullPointerException e){
            return null;
        }finally{
            try{
                if (connection != null && (!connection.isClosed())){
                    connection.close();
                }
            }catch(SQLException e){
                //e.printStackTrace();
                return null;
            }
        }
        return athleteLeader;
    }
}
