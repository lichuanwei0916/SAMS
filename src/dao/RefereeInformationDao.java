package dao;

import modle.JDBCUtil;
import modle.Referee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class RefereeInformationDao {
    private Connection connection = null;
    public Referee getRefereeInformation(String account) {
        Referee referee = new Referee();
        PreparedStatement pstmt = null;//声明一个PreparedStatement对象并将其初始化为null
        String sql = "select * from v_referee_information where Account = ?";//参数占位符
        ResultSet rs = null;
        try{
            connection = JDBCUtil.getConn();
            pstmt = connection.prepareStatement(sql,ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
            //ResultSet.TYPE_SCROLL_INSENSITIVE：游标可以前后移动，结果集对创建结果集后其他人对数据库所做的更改不敏感
            // 结果集是只读的，这是默认的并发类型
            pstmt.setString(1, account);
            rs = pstmt.executeQuery();
            rs.next();
            referee.setId(rs.getInt("R_ID"));
            referee.setName(rs.getString("R_Name"));
            referee.setNo(rs.getString("R_No"));
            referee.setGender(rs.getString("R_Gender"));
            referee.setTel(rs.getString("R_Tel"));
            referee.setAge(rs.getInt("R_Age"));
            pstmt.close();
            connection.close();
        }catch (SQLException e) {
            //e.printStackTrace();
            return null;
        }
        catch (NullPointerException e){
            return null;
        }finally{
            try{
                if (connection != null && (!connection.isClosed())){
                    connection.close();
                }
            }catch(SQLException e){
                //e.printStackTrace();
                return null;
            }
        }
        return referee;
    }
}
