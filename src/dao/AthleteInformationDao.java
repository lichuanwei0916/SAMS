package dao;

import modle.Athlete;
import modle.JDBCUtil;

import java.sql.*;

public class AthleteInformationDao {
    private Connection connection = null;
    public Athlete getAthleteInformation(String account) {
        Athlete athlete = new Athlete();
        PreparedStatement pstmt = null;//声明一个PreparedStatement对象并将其初始化为null
        String sql = "select * from v_athlete_information where Account = ?";//参数占位符
        ResultSet rs = null;
        try{
            connection = JDBCUtil.getConn();
            pstmt = connection.prepareStatement(sql,ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
            //ResultSet.TYPE_SCROLL_INSENSITIVE：游标可以前后移动，结果集对创建结果集后其他人对数据库所做的更改不敏感
            // 结果集是只读的，这是默认的并发类型
            pstmt.setString(1, account);
            rs = pstmt.executeQuery();
            rs.next();
            athlete.setId(rs.getInt("A_ID"));
            athlete.setName(rs.getString("A_Name"));
            athlete.setNo(rs.getString("A_No"));
            athlete.setGender(rs.getString("A_Gender"));
            athlete.setBirthDate(rs.getDate("A_Birth"));
            athlete.setTel(rs.getString("A_Tel"));
            athlete.setCollege(rs.getString("College"));
            athlete.setGroup(rs.getString("Group"));
            pstmt.close();
            connection.close();
        }catch (SQLException e) {
            //e.printStackTrace();
            return null;
        }
        catch (NullPointerException e){
            return null;
        }finally{
            try{
                if (connection != null && (!connection.isClosed())){
                    connection.close();
                }
            }catch(SQLException e){
                //e.printStackTrace();
                return null;
            }
        }
        return athlete;
    }

    public boolean updateTel_Bir(String tel,String bir,int id){
        PreparedStatement pstmt = null;//声明一个PreparedStatement对象并将其初始化为null
        String sql = "UPDATE athlete SET A_Tel = ?,A_Birth = ? WHERE A_ID = ?";//参数占位符
        ResultSet rs = null;
        JDBCUtil db = new JDBCUtil();
        int result = 0,status = 0;
        try{
            connection = db.getConn();
            pstmt = connection.prepareStatement(sql,ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
            //ResultSet.TYPE_SCROLL_INSENSITIVE：游标可以前后移动，结果集对创建结果集后其他人对数据库所做的更改不敏感
            // 结果集是只读的，这是默认的并发类型
            pstmt.setString(1, tel);
            pstmt.setDate(2, Date.valueOf(bir));
            pstmt.setInt(3, id);
            pstmt.executeUpdate();
            pstmt.close();
            connection.close();
        }catch (SQLException e) {
            //e.printStackTrace();
        }
        catch (NullPointerException e){
            return false;
        }finally{
            try{
                if (connection != null && (!connection.isClosed())){
                    connection.close();
                }
            }catch(SQLException e){
                //e.printStackTrace();
            }
        }
        return true;
    }
}
