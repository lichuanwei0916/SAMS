package dao;

import modle.HeadReferee;
import modle.JDBCUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class HeadRefereeInformationDao {
    private Connection connection = null;
    public HeadReferee getHeadRefereeInformation(String account) {
        HeadReferee headReferee = new HeadReferee();
        PreparedStatement pstmt = null;//声明一个PreparedStatement对象并将其初始化为null
        String sql = "select * from v_headreferee_information where Account = ?";//参数占位符
        ResultSet rs = null;
        try{
            connection = JDBCUtil.getConn();
            pstmt = connection.prepareStatement(sql,ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
            //ResultSet.TYPE_SCROLL_INSENSITIVE：游标可以前后移动，结果集对创建结果集后其他人对数据库所做的更改不敏感
            // 结果集是只读的，这是默认的并发类型
            pstmt.setString(1, account);
            rs = pstmt.executeQuery();
            rs.next();
            headReferee.setId(rs.getInt("H_ID"));
            headReferee.setName(rs.getString("H_Name"));
            headReferee.setNo(rs.getString("H_No"));
            headReferee.setGender(rs.getString("H_Gender"));
            headReferee.setTel(rs.getString("H_Tel"));
            headReferee.setAge(rs.getInt("H_Age"));
            pstmt.close();
            connection.close();
        }catch (SQLException e) {
            //e.printStackTrace();
            return null;
        }
        catch (NullPointerException e){
            return null;
        }finally{
            try{
                if (connection != null && (!connection.isClosed())){
                    connection.close();
                }
            }catch(SQLException e){
                //e.printStackTrace();
                return null;
            }
        }
        return headReferee;
    }
}
