package dao;

import modle.GroupInformation;
import modle.JDBCUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class GroupInformationDao {
    private Connection connection = null;
    public List<GroupInformation> getSearchItemInformation() {
        List<GroupInformation> list = new ArrayList<>();
        PreparedStatement pstmt = null;//声明一个PreparedStatement对象并将其初始化为null
        StringBuilder sql = new StringBuilder("select * from groupz WHERE 1=1");//参数占位符

        ResultSet rs = null;
        try{
            connection = JDBCUtil.getConn();
            pstmt = connection.prepareStatement(sql.toString(),ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
            //ResultSet.TYPE_SCROLL_INSENSITIVE：游标可以前后移动，结果集对创建结果集后其他人对数据库所做的更改不敏感
            // 结果集是只读的，这是默认的并发类型
            rs = pstmt.executeQuery();
            int num = 1;
            while (rs.next()) {
                GroupInformation groupInfo = new GroupInformation();
                groupInfo.setId(rs.getInt("G_ID"));
                groupInfo.setName(rs.getString("G_NAME"));
                list.add(groupInfo);
            }
            pstmt.close();
            connection.close();
        }catch (SQLException e) {
            //e.printStackTrace();
        }
        catch (NullPointerException e){
            return null;
        }finally{
            try{
                if (connection != null && (!connection.isClosed())){
                    connection.close();
                }
            }catch(SQLException e){
                //e.printStackTrace();
            }
        }
        return list;
    }
}
