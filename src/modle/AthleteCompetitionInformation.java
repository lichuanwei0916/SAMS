package modle;

import java.text.SimpleDateFormat;
import java.util.Date;

public class AthleteCompetitionInformation {
    private int id;
    private int num;
    private String a_name;//运动员姓名
    private int a_id;//运动员id
    private String a_no;//运动员学号
    private String a_gender;//运动员性别
    private int a_college_id;//运动员学院id
    private String a_college_name;//运动员学院名字
    private String a_tel;//运动员联系电话
    private int a_group_id;//运动员学院id
    private String a_group_name;//运动员学院
    private int ci_id;//比赛id
    private int r_id;//裁判id
    private int i_id;//检录员id
    private int recordingStatus;//检录状态
    private Date recordingTime;//检录时间
    private int group;//组号
    private int runway;//跑道
    private String grade;//成绩

    public AthleteCompetitionInformation() {
    }

    public AthleteCompetitionInformation(int id, int num, String a_name, int a_id, String a_no, String a_gender, int a_college_id, String a_college_name, String a_tel, int a_group_id, String a_group_name, int ci_id, int r_id, int i_id, int recordingStatus, Date recordingTime, int group, int runway, String grade) {
        this.id = id;
        this.num = num;
        this.a_name = a_name;
        this.a_id = a_id;
        this.a_no = a_no;
        this.a_gender = a_gender;
        this.a_college_id = a_college_id;
        this.a_college_name = a_college_name;
        this.a_tel = a_tel;
        this.a_group_id = a_group_id;
        this.a_group_name = a_group_name;
        this.ci_id = ci_id;
        this.r_id = r_id;
        this.i_id = i_id;
        this.recordingStatus = recordingStatus;
        this.recordingTime = recordingTime;
        this.group = group;
        this.runway = runway;
        this.grade = grade;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getA_name() {
        return a_name;
    }

    public void setA_name(String a_name) {
        this.a_name = a_name;
    }

    public int getA_id() {
        return a_id;
    }

    public void setA_id(int a_id) {
        this.a_id = a_id;
    }

    public String getA_no() {
        return a_no;
    }

    public void setA_no(String a_no) {
        this.a_no = a_no;
    }

    public String getA_gender() {
        return a_gender;
    }

    public void setA_gender(String a_gender) {
        this.a_gender = a_gender;
    }

    public int getA_college_id() {
        return a_college_id;
    }

    public void setA_college_id(int a_college_id) {
        this.a_college_id = a_college_id;
    }

    public String getA_college_name() {
        return a_college_name;
    }

    public void setA_college_name(String a_college_name) {
        this.a_college_name = a_college_name;
    }

    public String getA_tel() {
        return a_tel;
    }

    public void setA_tel(String a_tel) {
        this.a_tel = a_tel;
    }

    public int getA_group_id() {
        return a_group_id;
    }

    public void setA_group_id(int a_group_id) {
        this.a_group_id = a_group_id;
    }

    public String getA_group_name() {
        return a_group_name;
    }

    public void setA_group_name(String a_group_name) {
        this.a_group_name = a_group_name;
    }

    public int getCi_id() {
        return ci_id;
    }

    public void setCi_id(int ci_id) {
        this.ci_id = ci_id;
    }

    public int getR_id() {
        return r_id;
    }

    public void setR_id(int r_id) {
        this.r_id = r_id;
    }

    public int getI_id() {
        return i_id;
    }

    public void setI_id(int i_id) {
        this.i_id = i_id;
    }

    public String getRecordingStatus() {
        switch (recordingStatus){
            case 0:
                return "未检录";
            case 1:
                return "已检录";
        }
        return "----";
    }

    public void setRecordingStatus(int recordingStatus) {
        this.recordingStatus = recordingStatus;
    }

    public String getRecordingTime() {
        if(recordingTime == null)
            return "----";
        else{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String strDate = sdf.format(recordingTime);
            return strDate;
        }
    }

    public void setRecordingTime(Date recordingTime) {
        this.recordingTime = recordingTime;
    }

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    public int getRunway() {
        return runway;
    }

    public void setRunway(int runway) {
        this.runway = runway;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }
}
