package modle;

public class ItemInformation {
    private int id;//运动会项目编号
    private int num;//项目序号
    private String name;//运动会项目名字
    private int category;//运动会项目属性  个人赛为0，团体赛为1
    private int isFinals;//运动会项目是否预决赛    需要为0，不需要为1
    private String introduction;//运动会项目文字描述
    public ItemInformation(){
    }

    public ItemInformation(int id, int num, String name, int category, int isFinals, String introduction) {
        this.id = id;
        this.num = num;
        this.name = name;
        this.category = category;
        this.isFinals = isFinals;
        this.introduction = introduction;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        switch (category) {
            case 0:
                return "个人赛";
            case 1:
                return "团体赛";
        }
        return "";
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public String getIsFinals() {
        switch (isFinals) {
            case 0:
                return "预赛和决赛";
            case 1:
                return "决赛";
        }
        return "";
    }

    public void setIsFinals(int isFinals) {
        this.isFinals = isFinals;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }
}
