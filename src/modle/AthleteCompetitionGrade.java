package modle;

import java.text.SimpleDateFormat;
import java.util.Date;

public class AthleteCompetitionGrade {
    private int id;
    private int num;
    private String a_name;
    private String si_name;
    private int group;//组号
    private int runway;//跑道
    private String grade;//成绩
    private int recordingStatus;//检录状态
    private Date ci_time;//比赛时间

    public AthleteCompetitionGrade() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getA_name() {
        return a_name;
    }

    public void setA_name(String a_name) {
        this.a_name = a_name;
    }

    public String getSi_name() {
        return si_name;
    }

    public void setSi_name(String si_name) {
        this.si_name = si_name;
    }

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    public int getRunway() {
        return runway;
    }

    public void setRunway(int runway) {
        this.runway = runway;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getRecordingStatus() {
        switch (recordingStatus){
            case 0:
                return "未检录";
            case 1:
                return "已检录";
        }
        return "----";
    }

    public void setRecordingStatus(int recordingStatus) {
        this.recordingStatus = recordingStatus;
    }

    public String getCi_time() {
        if(ci_time == null)
            return "";
        else{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String strDate = sdf.format(ci_time);
            return strDate;
        }
    }

    public void setCi_time(Date ci_time) {
        this.ci_time = ci_time;
    }
}
