package modle;

import java.text.SimpleDateFormat;
import java.util.Date;

public class RefereeCompetitionInformation {
    private int id;//编号
    private int num;//序号
    private int ci_id;//比赛id
    private int r_id;//裁判id
    private String r_name;//裁判名字
    private Date startTime;//项目开始时间
    private String venue;//比赛地点
    private int si_id;//项目序号
    private String si_name;//项目名字
    private String introduction;//项目说明
    private String groupName;//组别名字
    private String nature;//项目属性
    private int issue;//运动会成绩是否发布

    public RefereeCompetitionInformation() {
    }

    public RefereeCompetitionInformation(int id, int ci_id, int r_id, String r_name, Date startTime, String venue, int si_id, String si_name, String introduction, String groupName, String nature, int issue) {
        this.id = id;
        this.ci_id = ci_id;
        this.r_id = r_id;
        this.r_name = r_name;
        this.startTime = startTime;
        this.venue = venue;
        this.si_id = si_id;
        this.si_name = si_name;
        this.introduction = introduction;
        this.groupName = groupName;
        this.nature = nature;
        this.issue = issue;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public int getCi_id() {
        return ci_id;
    }

    public void setCi_id(int ci_id) {
        this.ci_id = ci_id;
    }

    public int getR_id() {
        return r_id;
    }

    public void setR_id(int r_id) {
        this.r_id = r_id;
    }

    public String getR_name() {
        return r_name;
    }

    public void setR_name(String r_name) {
        this.r_name = r_name;
    }

    public String getStartTime() {
        if(startTime == null)
            return "";
        else{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String strDate = sdf.format(startTime);
            return strDate;
        }
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public int getSi_id() {
        return si_id;
    }

    public void setSi_id(int si_id) {
        this.si_id = si_id;
    }

    public String getSi_name() {
        return si_name;
    }

    public void setSi_name(String si_name) {
        this.si_name = si_name;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public String getIssue() {
        switch (issue){
            case 0:
                return "否";
            case 1:
                return "是";
        }
        return "";
    }

    public void setIssue(int issue) {
        this.issue = issue;
    }
}
