package modle;

import java.text.SimpleDateFormat;
import java.util.Date;

public class CompetitionInformation {
    private int id;//比赛编号
    private int num;//序号
    private String venue;//比赛地点
    private Date time;//比赛开始时间
    private int itemId;//比赛项目id
    private String itemName;//比赛项目名字
    private int category;//运动会项目属性  个人赛为0，团体赛为1
    private int nature;//比赛性质,1为预赛，2为决赛，3为预决赛
    private int limit;//比赛每组限制人数
    private int group;//比赛组别
    private String groupName;//组别名字

    public CompetitionInformation() {
    }

    public CompetitionInformation(int id, String venue, Date time, int itemId, String itemName, int category, int nature, int limit, int group, String groupName) {
        this.id = id;
        this.venue = venue;
        this.time = time;
        this.itemId = itemId;
        this.itemName = itemName;
        this.category = category;
        this.nature = nature;
        this.limit = limit;
        this.group = group;
        this.groupName = groupName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public String getTime() {
        if(time == null)
            return "";
        else{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            String strDate = sdf.format(time);
            return strDate;
        }
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getCategory() {
        switch (category) {
            case 0:
                return "个人赛";
            case 1:
                return "团体赛";
        }
        return "";
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public String getNature() {
        switch (nature) {
            case 1:
                return "预赛";
            case 2:
                return "决赛";
            case 3:
                return "预决赛";
        }
        return "";
    }

    public void setNature(int nature) {
        this.nature = nature;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

}
