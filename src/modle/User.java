package modle;

public class User {
    private String username;
    private String roletype;

    public User() {
    }

    public User(String username, String roletype) {
        this.username = username;
        this.roletype = roletype;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRoletype() {
        return roletype;
    }

    public void setRoletype(String roletype) {
        this.roletype = roletype;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", roletype='" + roletype + '\'' +
                '}';
    }
}
