package modle;

import java.text.SimpleDateFormat;
import java.util.Date;

public class AthleteSignUpRecord {
    private int id;//报名id
    private int num;//序号
    private String name;//姓名
    private String no;//学/工号
    private String event;//报名项目
    private Date signUpDate;//报名时间
    private String status;//审核状态
    private Date auditDate;//审核时间
    private String bestGrade;//历史最好成绩
    public AthleteSignUpRecord() {
    }

    public AthleteSignUpRecord(int id, String name, String no, String event, Date signUpDate, String status, Date auditDate, String bestGrade) {
        this.id = id;
        this.name = name;
        this.no = no;
        this.event = event;
        this.signUpDate = signUpDate;
        this.status = status;
        this.auditDate = auditDate;
        this.bestGrade = bestGrade;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getSignUpDate() {
        if(signUpDate == null)
            return "";
        else{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String strDate = sdf.format(signUpDate);
            return strDate;
        }
    }

    public void setSignUpDate(Date signUpDate) {
        this.signUpDate = signUpDate;
    }

    public String getStatus() {
        switch (status) {
            case "0":
                return "待审核";
            case "1":
                return "未通过";
            case "2":
                return "已通过";
        }
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAuditDate() {
        if(auditDate == null)
            return "----";
        else{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String strDate = sdf.format(auditDate);
            return strDate;
        }
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public void setAuditDate(Date auditDate) {
        this.auditDate = auditDate;
    }

    public String getBestGrade() {
        return bestGrade;
    }

    public void setBestGrade(String bestGrade) {
        this.bestGrade = bestGrade;
    }
}
