package modle;

public class Referee {
    private int id;//自增长id
    private String name;//裁判长姓名
    private String no;//裁判长学号/工号
    private String gender;//裁判长性别
    private int age;//裁判长年龄
    private String tel;//裁判长联系电话

    public Referee() {
    }

    public Referee(int id, String name, String no, String gender, int age, String tel) {
        this.id = id;
        this.name = name;
        this.no = no;
        this.gender = gender;
        this.age = age;
        this.tel = tel;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }
}
