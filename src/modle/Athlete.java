package modle;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Athlete{
    private int id;//自增长id
    private String name;//运动员姓名
    private String no;//运动员学号/工号
    private String gender;//运动员性别
    private Date birthDate;//运动员生日
    private String tel;//运动员联系电话
    private String college;//运动员学院
    private String group;//运动员组别

    public Athlete() {
        super();
    }

    public Athlete(int id, String name, String no, String gender, Date birthDate, String tel, String college, String group) {
        this.id = id;
        this.name = name;
        this.no = no;
        this.gender = gender;
        this.birthDate = birthDate;
        this.tel = tel;
        this.college = college;
        this.group = group;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthDate() {
        if(birthDate == null)
            return "";
        else{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String strDate = sdf.format(birthDate);
            return strDate;
        }
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getTel() {
        if(tel == null)
            tel = "";
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    @Override
    public String toString() {
        return "Athlete{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", node='" + no + '\'' +
                ", gender='" + gender + '\'' +
                ", birthDate=" + birthDate +
                ", tel='" + tel + '\'' +
                ", college='" + college + '\'' +
                ", group='" + group + '\'' +
                '}';
    }
}
